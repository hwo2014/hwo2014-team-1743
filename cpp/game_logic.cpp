#include "game_logic.h"
#include "protocol.h"
#include "math.h"
#include <deque>
#include <algorithm>
#include <boost/timer.hpp>


using namespace hwo_protocol;

namespace
{
    
    /************************PRIVATE MEMBERS******************************/   
    //track information 
    jsoncons::json   TRACK_INFO; 
    //info about current position of the car
    jsoncons::json   CAR_POSITION;
    //info about current position of all the cars
    jsoncons::json   CAR_POSITIONS;
    
    double TEMP_TARGET_SPEED;
    int TEMP_PREV_CORNER_INDEX;
    
    
    //the list stores latest car data
    std::deque<std::map<int, double>> RACE_LOG;
    std::map<int, double> CURRENT_TICK_LOG;
    int LOG_SIZE = 3;
    //enums used for keys in the maps in above list
    int SLIP_ANGLE = 1;
    int PIECE_INDEX = 2;
    int PIECE_DIST = 3;
    int SPEED = 4;
    int THROTTLE = 5;
    int START_LANE = 6;
    int END_LANE = 7;
    
    int SLIDE_SPEED = 100;
    int SLIDE_WIDTH = 101;
    int SLIDE_ACC = 102;
    int LAP = 103;
    
    //needs to store the information about v and a at the beginning of the corner
    //first key is the radius of the piece
    //third key is the CORNER_DATA_KEY
    //then is the value for given CORNER_DATA_KEY    
    std::map<int, std::map<int, double>> CORNER_DATA;
    //enums for keys in the maps in above map :)
    int V1 = 1;
    int A1 = 2;
    int V2 = 3;
    int A2 = 4;
    
    double LAST_CORNER_SPEED = 0;
    int LAST_CORNER_RADIUS = 0;
    
    bool REQ_SWITCH = false;  
    bool TURBO_READY = false;
    double TURBO_FACTOR = 1;
    double USED_TURBO_FACTOR = 1;
    bool TURBO_IN_USE = false;
    int TURBO_TICKS = 0;
    std::string CAR_COLOR;
    
    double TRACK_MAX_ANGLE = 0;
    
    //if bumpped or not done full lap, the tracking is invalid
    bool TRACK_MAX_INVALID = true;
    
    /************************CALCULATED VALUES******************************/        
    
    //stores the information about the maximum cruising speed for given corner radius and lane
    //first key is the radius
    //first in pair is the entry speed
    //second in pair is the cruising speed
    std::map<int, std::map<int, double>> CORNER_SPEED;
    std::map<int, double> TEMP_CORNER_SPEED_ENTRY;
    int ENTRY_SPEED = 1;
    int EXIT_SPEED = 2;
    int ENTRY_ANGLE = 3;
    int EXIT_ANGLE = 4;
    int MAX_ANGLE = 5;
    int CORNER_RADIUS = 6;
    int TARGET_SPEED = 7;
    int PREVIOUS_CORNER_EXIT_ANGLE = 8;
    
    
    double BASE_RADIUS = 0;
    double BASE_SPEED = 0;
    
    double ENGINE_POWER = 0;
    double ACCELERATION_COEFF = 0;
    double SLIDE_TORQUE = 0;
    double MAX_SLIDE = 8.0;
    
    //contains a list of all switch pieces on the track and the distance to the next switch including the next switch
    std::map<int, int> SWITCH_MAP;
    
    //acceleration lists
    
    std::map<int, double> ACC_DIST_MAP;
    std::map<double, int> DIST_ACC_MAP;
    
    double CURR_ACC_DIST = 0;
    int CURR_ACC_COUNT = 0;
    bool TRACKING_ACC = false;
    
    std::map<std::string, int> LAP_TIMES;
    
    
    
    /************************UTILITY FUNCTIONS******************************/    
    
    //return the index of the next piece
    int getNextPieceIndex(int currentIndex, int count)
    {
        return currentIndex == count - 1 ? 0 : currentIndex + 1;
    }
    
    //returns the engine power,
    //v1 and v2 are the speeds read at 2 consecutives ticks, when throttle = 1;
    double getEnginePower(double v1, double v2)
    {
        double accCoeff = v1 / ( v1 * 2 - (v2 + v1));
        return (v2 - v1) * accCoeff + v1;
    }
    
    //returns the speed at which the car will not slide
    //parameters v1 and a1 must be read at the first tick with dist in piece > the distance between guide flag and the center of the car.
    //also the slip angle on the previous tick should be as close as possible to 0, otherwise the calculation get inaccurate
    //v2 and a1 must be measured the same way for the same kind of a corner, on the same "lane"
    double getTresholdSpeed(double v1, double a1, double v2, double a2)
    {
        //         std::cout
        //         << "v1: " << v1 
        //         << ", a1: " << a1 
        //         << ", v2: " << v2 
        //         << ", a2: " << a2 
        //         << std::endl;
        
        double slideCoeff = (a1 - a2) / (v1 - v2);
        //                 std::cout << "Got slideCoeff: " << slideCoeff <<  std::endl;
        return v1 - a1 / slideCoeff;
    }
    
    //returns the distance needed to slowdown from speed vStart to vStop
    double getBreakDistance(double vStart, double vStop, double accCoeff)
    {
        return accCoeff * (vStart - vStop);
    }
    
    //returns the speed at which the car will not slide on a corner with given radius
    //baseRadius is a radius of a corner for which the treshold speed has been already calculated
    //baseSpeed is the treshold speed of the corner with baseRadius
    //radius is the corner radius for which we want to calculate the sped treshold
    double getTresholdSpeedForRadius(double baseRadius, double baseSpeed, double radius)
    {
        double retVal = 0;
        if(baseRadius > 0.1)
        {
            retVal = sqrt(radius/baseRadius) * baseSpeed;
        }
        //             std::cout << "baseRadius: " << baseRadius 
        //             << "baseSpeed: " << baseSpeed 
        //             << "radius: " << radius 
        //             << "retVal: " << retVal 
        //             <<  std::endl;
        return retVal;
    }
    
    //log current data to the race log
    void setLogValue(int type, double value)
    {
        CURRENT_TICK_LOG.insert(std::pair<int,double>(type, value));
    }
    
    double getLogValue(int type, double defaultValue)
    {
        double retVal = defaultValue;
        if(CURRENT_TICK_LOG.count(type) != 0)
        {
            retVal = CURRENT_TICK_LOG[type];
        }
        return retVal;
    }
    
    int getPieceRadius(int index, int lane)
    {
        int retVal = 0;
        
        const auto& piece_radius = TRACK_INFO["pieces"][index].get("radius", 0).as<double>();
        if(piece_radius > 0.1)
        {
            const auto& piece_angle = TRACK_INFO["pieces"][index].get("angle", 0).as<double>();
            if(piece_angle < 0)
            {
                // 	std::cout << "lane" << lane << std::endl;
                retVal = piece_radius + TRACK_INFO["lanes"][lane]["distanceFromCenter"].as<double>();
            }
            else
            {
                // 	std::cout << "lane" << lane << std::endl;
                retVal = piece_radius - TRACK_INFO["lanes"][lane]["distanceFromCenter"].as<double>();
            }
        }
        
        return retVal;
    }
    
    int getNextCornerIndex(int cur_index)
    {
        int retVal = cur_index;
        int nextCornerValid = false;
        int currentAngle = TRACK_INFO["pieces"][cur_index].get("angle", 0).as<int>();
        int piece_count = TRACK_INFO["pieces"].size();
        for(int i = 1; i <=  piece_count; ++i)
        {
            if(cur_index + i < piece_count)
            {
                retVal = cur_index + i;
            }
            else
            {
                retVal = cur_index + i - piece_count;
            }
            auto next_cornerAngle = TRACK_INFO["pieces"][retVal].get("angle", 0).as<int>();
            if(next_cornerAngle != 0 )
            {
                if(next_cornerAngle != currentAngle)
                {
                    nextCornerValid = true;
                }
                if(nextCornerValid)
                    break;
            }
            else
            {
                nextCornerValid = true;
            }
        }
        return retVal;
    }
    
    double getPieceLength(int index, int start_lane, int end_lane)
    {
        double retVal = 0;
        double radius = getPieceRadius(index, start_lane);
        
        if(radius != 0)
        {
            retVal = radius /( 180 / abs(TRACK_INFO["pieces"][index].get("angle", 0).as<double>()) / 3.1415926535897932384626433832795);
        }
        else
        {
            retVal = TRACK_INFO["pieces"][index].get("length", 0).as<double>();
        }
        
        if(start_lane != end_lane)
        {
            retVal = retVal * 1.019;
        }
        
        return retVal;
    }
    
    
    void presetCornerLimits(int base_radius, int base_speed)
    {
        int piece_count = TRACK_INFO["pieces"].size();
        for(int i = 0; i < piece_count; ++i)
        {
            
        }
    }
    
    //log corner data to the race log
    void setCornerData(int radius, double angle, double v, double a)
    {
        //first we need to prepare the radius and lane data based on the angle
        
        if(angle < 0)
        {
            a = -a;
        }
        
        
        auto radius_map = CORNER_DATA[radius];
        bool calculate_corner;
        if(radius_map.count(V1) != 0 && radius_map.count(A1) != 0)
        {
            radius_map[V2] = v;
            radius_map[A2] = a;
            BASE_RADIUS = radius;
            BASE_SPEED = getTresholdSpeed(radius_map[V1], radius_map[A1], radius_map[V2], radius_map[A2]);
//             std::cout << "Got speed: " << BASE_SPEED << ", for radius = " << radius<<  std::endl;
        }
        else
        {
            //             std::cout << "Pre-Setting corner speed: " << radius_map[V1] << ", angle: " << radius_map[A1] << ", for radius: " << radius << std::endl;
            radius_map[V1] = v;
            radius_map[A1] = a;
            //             std::cout << "Setting corner speed: " << v << ", angle: " << a << ", for radius: " << radius << std::endl;
        }
        
        CORNER_DATA[radius] = radius_map;  
    }
    
    
    
    ////////////////////OTHER FUNCTIONS////////////////////////////////////////
    void checkEnginePower()
    {
        if(ENGINE_POWER == 0 && RACE_LOG.size() >= 2)
        {
            double v1 = RACE_LOG.at(RACE_LOG.size() - 2)[SPEED];
            double v2 = RACE_LOG.at(RACE_LOG.size() - 1)[SPEED];
            if( 
                v2 > v1 &&
                v1 != 0 && RACE_LOG.at(RACE_LOG.size() - 2)[THROTTLE] == 1.0 &&
                v2 != 0 && RACE_LOG.at(RACE_LOG.size() - 1)[THROTTLE] == 1.0
            )
            {
                ACCELERATION_COEFF = v1 / ( v1 * 2 - v2);
                std::cout << "Got ACCELERATION_COEFF: " << ACCELERATION_COEFF <<  std::endl;
                ENGINE_POWER = v1 * ACCELERATION_COEFF;
                std::cout << "Got ENGINE_POWER: " << ENGINE_POWER <<  std::endl;
            }
        }
    }
    
    
    double getDistToPiece(int index)
    {
        double retVal = 0;
        double piece_index = getLogValue(PIECE_INDEX, 0);
        double piece_dist = getLogValue(PIECE_DIST, 0);
        double start_lane = getLogValue(START_LANE, 0);
        double end_lane = getLogValue(END_LANE, 0);
        double piece_length = getPieceLength(piece_index, start_lane, end_lane);
        double piece_count = TRACK_INFO["pieces"].size();
        
        retVal = piece_length - piece_dist;
        
        for(int j = piece_index; j < (piece_index < index ? index : index + piece_count); ++j)
        {
            int actualIndex = getNextPieceIndex(piece_index, piece_count);
            double piece_length = getPieceLength(actualIndex, start_lane, end_lane);
            retVal = retVal + piece_length;
        }
        
        return retVal;
    }
    
    
    double checkCornerSpeed()
    {
        double retVal = 0;
        double target_speed = ENGINE_POWER / 5;
        
        if(LAST_CORNER_SPEED < 0.1)
        {
            //assume the min torque = 0.29
            LAST_CORNER_RADIUS = 100;
            LAST_CORNER_SPEED = sqrt(0.29 * LAST_CORNER_RADIUS);
        }
        
        if(SLIDE_TORQUE == 0)
        {
            double curr_slide = getLogValue(SLIP_ANGLE, 0);
            if(curr_slide != 0)
            {
                double slipSpeed = LAST_CORNER_SPEED;
                SLIDE_TORQUE = slipSpeed * slipSpeed / LAST_CORNER_RADIUS;
                std::cout << "Got SLIDE_TORQUE: " << SLIDE_TORQUE <<  std::endl;
            }
            else
            {
                
                double piece_index = getLogValue(PIECE_INDEX, 0);
                double piece_radius = getPieceRadius(piece_index, getLogValue(START_LANE, 0));
                double next_corner_index = getNextCornerIndex(piece_index);
                double next_corner_radius = getPieceRadius(next_corner_index, getLogValue(START_LANE, 0));
                double dist_next_corner = getDistToPiece(next_corner_index);
                double tresholdSpeed = getTresholdSpeedForRadius(LAST_CORNER_RADIUS, LAST_CORNER_SPEED, piece_radius);
                if(LAST_CORNER_RADIUS != 0 && LAST_CORNER_SPEED != 0)
                {
                    
                    if(piece_radius > 0.1)
                    {
                        target_speed = LAST_CORNER_SPEED + 0.1;
                        // 	    std::cout << "0   " << LAST_CORNER_RADIUS << "...." << LAST_CORNER_SPEED << std::endl;
                    }
                    //This means, we have been already cruising at some corner
                    
                    //  	   std::cout << "next_corner_index   " << next_corner_index << std::endl;
                    double next_corner_speed = getTresholdSpeedForRadius(LAST_CORNER_RADIUS, LAST_CORNER_SPEED, next_corner_radius);
                    
                    double gowno = (next_corner_speed - target_speed);//getLogValue(SPEED, 0);
                    
                    if((gowno > 0.1) && (gowno < dist_next_corner) || piece_radius == 0)
                    {
                        // 	    std::cout << "1   " << std::endl;
                        target_speed = next_corner_speed;
                    }
                }
                
                if(piece_radius > 0.1 && getLogValue(SPEED,0) >= tresholdSpeed)
                {
                    LAST_CORNER_RADIUS = piece_radius;
                    LAST_CORNER_SPEED = getLogValue(SPEED, 0);
                }
            }
        }
        
        const auto& ca_speed_target_current_diff = target_speed - getLogValue(SPEED, 0);
        double maxAcc = (ENGINE_POWER - getLogValue(SPEED, 0));
        retVal = ca_speed_target_current_diff / maxAcc / 2 * ACCELERATION_COEFF + target_speed/ENGINE_POWER;
        
        retVal = retVal < 0 ? 0.0 : (retVal >= 1 ? 1.0 : retVal);
        
        return retVal;
    }
    
    double getAdjustedSpeed(bool alteranativeLane, int direction)
    {
        double retVal = 1.0;
        
        if(ENGINE_POWER > 0.01)
        {
            
            // first add speed for maxxpeed
            double speedDivisor = ACCELERATION_COEFF;
            double maxSlide = 0;
            
            double car_speed = getLogValue(SPEED,0);
            
            
            double car_acc = (USED_TURBO_FACTOR * ENGINE_POWER - car_speed)/speedDivisor;
            double car_inPieceDist = getLogValue(PIECE_DIST,0);
            double car_piece = getLogValue(PIECE_INDEX,0);
            int car_startLane = getLogValue(START_LANE,0);
            int car_endLane = getLogValue(END_LANE,0);
            double car_slideWidth = getLogValue(SLIDE_WIDTH, 0);
            double car_slideAcc = getLogValue(SLIDE_ACC, 0);
            double car_slideSpeed = getLogValue(SLIDE_SPEED, 0);
            double car_lap = getLogValue(LAP,0);
            double car_slide_angle = getLogValue(SLIP_ANGLE,0);
            
            double m_tInfo_pieceCount = TRACK_INFO["pieces"].size();
            double m_torque = SLIDE_TORQUE; //This must be automatically detected
            double m_multiplier = 0.8;
            double m_springConst = 0.0135;
            double m_damping = 0.9;
            bool switch_done = false;
            bool printInfo = false;
            int lane_count = TRACK_INFO["lanes"].size();
            
//             std::cout << "car_acc::::" << car_acc << std::endl;
            car_speed = car_speed + car_acc;
            
            //       if(car_piece < RACE_LOG.at(RACE_LOG.size() - 1)[PIECE_INDEX])
            //       {
            // 	printInfo = true;
            //       }
            
            int loopCount = 0;
	    bool alternativeSwtichDone = false;
            
            while (car_speed > 4) { // FIXME find the tightest curve and calculate the slip speed -> limit
                loopCount++;
                car_inPieceDist += car_speed;
                
                double track_length = getPieceLength(car_piece, car_startLane, car_endLane);
                
                if (car_inPieceDist > track_length) {
                    car_startLane = car_endLane;
                    car_inPieceDist -= track_length;
                    car_piece++;
                    if (car_piece >= m_tInfo_pieceCount) {
                        car_lap++;
                        car_piece = 0;
                    }
                    
                    if(SWITCH_MAP.count(car_piece) != 0)
                    {
                        double sum = SWITCH_MAP[car_piece];
			if(!alternativeSwtichDone && alteranativeLane)
			{
			  sum = direction;
			  alternativeSwtichDone = true;
			}
			
                        if(sum > 0){
                            car_endLane = std::min(car_startLane + 1, lane_count -1);
                        }else if(sum < 0){
                            car_endLane = std::max(car_startLane - 1, 0);
                        }
                    }
                }
                
                // normal acceleration mass == 1
                double cur_radius = getPieceRadius(car_piece, car_startLane);
                double normalAcc = 0;
                if(cur_radius > 0.01)
                {
                    double angle = TRACK_INFO["pieces"][car_piece].get("angle", 0).as<double>();
                    normalAcc = car_speed * car_speed / cur_radius * (angle/std::fabs(angle));
                }
                
                double torqueCoeff = 1.0;
                
                if (normalAcc > m_torque * torqueCoeff) {
                    normalAcc -= m_torque * torqueCoeff;
                }
                else if (normalAcc < - m_torque * torqueCoeff) {
                    normalAcc += m_torque * torqueCoeff;
                }
                else {
                    normalAcc = 0;
                }
                
                normalAcc *= m_multiplier;
                
                
                double springAcc = car_slideWidth * m_springConst;
                
                car_slideAcc = normalAcc - springAcc;
                
                car_slideSpeed += car_slideAcc;
                car_slideSpeed *= m_damping;
                
                car_slideWidth += car_slideSpeed;
                
                maxSlide = std::max(fabs(car_slideWidth), maxSlide);
                
                // 	if(printInfo)
                // 	{
                // 	  std::cout << "---;" 
                // 	  << car_speed << ";" 
                // 	  << car_acc << ";" 
                // 	  << car_slideWidth << ";" 
                // 	  << car_slideSpeed << ";" 
                // 	  << std::endl;
                // 	  
                // 	}
                // 	std::cout << car_startLane << "::::" << car_endLane << std::endl;
                
                //<< m_tInfo.radius(car_piece, car_startLane, car_endLane);
                if (maxSlide > MAX_SLIDE + SLIDE_TORQUE - loopCount*0.012) { //sqrt(cos(60))
                    // 	  std::cout << "---" << car_speed << car_acc << car_slideWidth << car_slideSpeed << std::endl;
                    return 0;
                }
                
                // now decrease the speed until we are done
                car_acc = car_speed/speedDivisor;
                car_speed -= car_acc;
            }
        }
        
        
        return retVal;
    }
    
    //indicate that new Tick is being processed
    void startNewTick(const jsoncons::json& msg)
    {
        const auto& data = msg["data"];
        jsoncons::json carPosition;
        for(int i=0; i < data.size(); ++i)
        {
            const auto& car_color = data[i]["id"]["color"].as<std::string>();
            if(car_color.compare(CAR_COLOR) == 0)
            {
                CAR_POSITION = data[i];
                break;
            }
        }  
        CAR_POSITIONS = data;
        
        if(RACE_LOG.size() >= LOG_SIZE)
            RACE_LOG.pop_front();
        
        if(CURRENT_TICK_LOG.size() != 0)
            RACE_LOG.push_back(CURRENT_TICK_LOG);
        
        
        
        CURRENT_TICK_LOG.clear();
        CURRENT_TICK_LOG.insert(std::pair<int,double>(SLIP_ANGLE, CAR_POSITION["angle"].as<double>()));
        CURRENT_TICK_LOG.insert(std::pair<int,double>(PIECE_INDEX, CAR_POSITION["piecePosition"]["pieceIndex"].as<double>()));
        CURRENT_TICK_LOG.insert(std::pair<int,double>(PIECE_DIST, CAR_POSITION["piecePosition"]["inPieceDistance"].as<double>()));
        CURRENT_TICK_LOG.insert(std::pair<int,double>(START_LANE, CAR_POSITION["piecePosition"]["lane"]["startLaneIndex"].as<double>()));
        CURRENT_TICK_LOG.insert(std::pair<int,double>(END_LANE, CAR_POSITION["piecePosition"]["lane"]["endLaneIndex"].as<double>()));
        CURRENT_TICK_LOG.insert(std::pair<int,double>(LAP, CAR_POSITION["piecePosition"]["lap"].as<double>()));
        

        
        
        int curr_piece_index = getLogValue(PIECE_INDEX, 0);
        if(RACE_LOG.size() == 0)
        {
            CURRENT_TICK_LOG.insert(std::pair<int,double>(SPEED, getLogValue(PIECE_DIST, 0)));
            
            CURRENT_TICK_LOG.insert(std::pair<int,double>(SLIDE_SPEED, 0));
            CURRENT_TICK_LOG.insert(std::pair<int,double>(SLIDE_WIDTH, 0));
            CURRENT_TICK_LOG.insert(std::pair<int,double>(SLIDE_ACC, 0));
        }
        else
        {
            /////////////////KARES MAGICAL STUFFF/////////////////////////
            double slideWidth = sin(getLogValue(SLIP_ANGLE,0)*3.14159265359/180)*10;
            double slideSpeed = slideWidth - RACE_LOG.at(RACE_LOG.size() - 1)[SLIDE_WIDTH];
            double slideAcc = slideSpeed - RACE_LOG.at(RACE_LOG.size() - 1)[SLIDE_SPEED];
            
            CURRENT_TICK_LOG.insert(std::pair<int,double>(SLIDE_SPEED, slideSpeed));
            CURRENT_TICK_LOG.insert(std::pair<int,double>(SLIDE_WIDTH, slideWidth));
            CURRENT_TICK_LOG.insert(std::pair<int,double>(SLIDE_ACC, slideAcc));
            
            ///////////////////////////////////////////////////////////////
            
            double speed = getLogValue(PIECE_DIST, 0) - RACE_LOG.at(RACE_LOG.size() - 1)[PIECE_DIST];
            int prev_piece_index = RACE_LOG.at(RACE_LOG.size() - 1)[PIECE_INDEX];
            const auto& prev_piece_radius = getPieceRadius(prev_piece_index, RACE_LOG.at(RACE_LOG.size() - 1)[START_LANE]);
            
            if(getLogValue(PIECE_INDEX, 0) != prev_piece_index)
            {
                double piece_length = getPieceLength(prev_piece_index, RACE_LOG.at(RACE_LOG.size() - 1)[START_LANE], RACE_LOG.at(RACE_LOG.size() - 1)[END_LANE]);
                
                speed = speed + piece_length;
            }
            
            CURRENT_TICK_LOG.insert(std::pair<int,double>(SPEED, speed));

            
            if(RACE_LOG.size() > 1 &&
                fabs(speed - RACE_LOG.at(RACE_LOG.size() - 1)[SPEED]) > (ENGINE_POWER * TURBO_FACTOR / ACCELERATION_COEFF) * 1.2 &&
                getLogValue(PIECE_INDEX, 0) == RACE_LOG.at(RACE_LOG.size() - 2)[PIECE_INDEX]
            )
            {
                std::cout << "Bumping, angle tracking disabled... " << std::endl;
                std::cout << "speed: " << speed << ", old_speed: " <<  RACE_LOG.at(RACE_LOG.size() - 1)[SPEED] << std::endl;
                std::cout << "index: " << getLogValue(PIECE_INDEX, 0) << ", dist: " <<  getLogValue(PIECE_DIST, 0) << std::endl;
                TRACK_MAX_INVALID = true;
            }
        
        }
        
        double absolute_slide_angle = fabs(getLogValue(SLIP_ANGLE,0));
        if(TRACK_MAX_ANGLE < absolute_slide_angle)
        {
            TRACK_MAX_ANGLE = absolute_slide_angle;
        }
        
        if(absolute_slide_angle > 59 && !TRACK_MAX_INVALID){
        
            MAX_SLIDE = MAX_SLIDE - 0.1;

            std::cout << "TRACK_MAX_ANGLE: " << TRACK_MAX_ANGLE << ", decrease MAX_SLIDE to: " << MAX_SLIDE << std::endl;
            TRACK_MAX_INVALID = true;
        }
        
    }
    
    bool safeToNotThrottle(double throttle)
    {
        return /*((RACE_LOG.at(RACE_LOG.size()-1)[THROTTLE] * ENGINE_POWER > getLogValue(SPEED,0) ? 1 : 0)
            == 
            (getLogValue(SPEED,0) > RACE_LOG.at(RACE_LOG.size()-1)[SPEED] ? 1 : 0)
            &&*/
            fabs(RACE_LOG.at(RACE_LOG.size()-1)[THROTTLE] - throttle) < 0.01;
    }
    
    int switchLanes(int piece_index, int start_lane, int end_lane)
    {            
        int retVal = 0;
        
        
        
        if(!REQ_SWITCH && SWITCH_MAP.size() > 0)
        {
            int angle_sum = 0;
            int lane_count = TRACK_INFO["lanes"].size();
            std::map<int,int>::iterator it=SWITCH_MAP.begin();
            for (; it!=SWITCH_MAP.end(); ++it)
            {
                angle_sum = it->second;
                if(it->first > piece_index)
                {
                    break;
                }
            }
            if(it == SWITCH_MAP.end())
            {
                angle_sum = SWITCH_MAP.begin()->second;
            }
            
            if(angle_sum < 0 && start_lane > 0 && start_lane == end_lane)
            {
                retVal = -1;
            }
            else if(angle_sum > 0 && start_lane < lane_count - 1 && start_lane == end_lane)
            {
                retVal = 1;
            }
        }
        
        return retVal;
    }
    
    int overtake()
    {
      int retVal = 0;
      
      if(SWITCH_MAP.size() > 0)
      {
	//check if car in front is slower
	//get our best lap time
	int our_top_lap_time = 0;
	if(LAP_TIMES.count(CAR_COLOR) != 0)
	{
	  our_top_lap_time = LAP_TIMES[CAR_COLOR];
	}
	
	//get next switch index
	std::map<int,int>::iterator it_first_switch = SWITCH_MAP.upper_bound(getLogValue(PIECE_INDEX,0));
	if(it_first_switch == SWITCH_MAP.end())
	{
	  it_first_switch = SWITCH_MAP.begin();
	}

	std::map<int,int>::iterator it_second_switch = SWITCH_MAP.upper_bound(it_first_switch->first);
	if(it_second_switch == SWITCH_MAP.end())
	{
	  it_second_switch = SWITCH_MAP.begin();
	}
	
	
	
	int pieceCount = TRACK_INFO["pieces"].size();
	int dist_between_switches = it_second_switch->first - it_first_switch->first;
	if(dist_between_switches < 0)
	  dist_between_switches = dist_between_switches + pieceCount;
	
	std::map<int, int> lane_danger_map; //keeps info how many obstacles per lane in short distance we have
	
	//iterate the opponents
	for(int i = 0; i < CAR_POSITIONS.size(); ++i)
	{
	  std::string car_color = CAR_POSITIONS[i]["id"]["color"].as<std::string>();
	  if(car_color.compare(CAR_COLOR) != 0)
	  {
	    int car_piece_index = CAR_POSITIONS[i]["piecePosition"]["pieceIndex"].as<int>();
	    double car_piece_dist = CAR_POSITIONS[i]["piecePosition"]["inPieceDistance"].as<double>();
	    int car_end_lane = CAR_POSITIONS[i]["piecePosition"]["lane"]["endLaneIndex"].as<int>();;
	    int car_lap_time = 0;
	    if(LAP_TIMES.count(car_color) != 0)
	    {
	      car_lap_time = LAP_TIMES[CAR_COLOR];
	    }
	    
	    //we have the info about the car, now check if we need to overtake it...
	    //overtake a car only if the distance between is smaller or equal the distance between next 2 switch pieces
	    int dist_between_cars = car_piece_index - getLogValue(PIECE_INDEX, 0);
	    if(dist_between_cars < 0)
	      dist_between_cars = dist_between_cars + TRACK_INFO["pieces"].size();
	    
	    
	    bool dangerZone = false;
	    
	    
	    
	    if(dist_between_cars == 0)
	    { 
	      double dist_diff = car_piece_dist - getLogValue(PIECE_DIST,0);

	      
	      if(dist_diff > 0 && dist_diff < 60)
	      {
		//probably need to switch immediately...
		dangerZone = true;
	      }
	    }
	    else if(dist_between_cars == 1)
	    {
	      double piece_length = getPieceLength(getLogValue(PIECE_INDEX,0), getLogValue(START_LANE,0), getLogValue(END_LANE,0));
	      
	      double dist_diff = car_piece_dist - getLogValue(PIECE_DIST,0) + piece_length;

	      if(dist_diff > 0 && dist_diff < 60)
	      {
		//probably need to switch immediately
		dangerZone = true;
	      }
	    }
	    
	    if(car_lap_time < our_top_lap_time)
	    {
		//we are slower
// 	   	  std::cout 
// 	    	      << "car_lap_time" << car_lap_time
// 	    	      << ", our_top_lap_time" << our_top_lap_time
// 	   	      << std::endl;
		dangerZone = false;
	    }

	    
	    if(dangerZone)
	    {
	      	         
	      int dangerCount = 0;
	      if(lane_danger_map.count(car_end_lane) != 0)
		dangerCount = lane_danger_map[car_end_lane];
		
	      lane_danger_map.insert(std::pair<int,int>(car_end_lane, dangerCount + 1));
	    }
	  }
	}
	
	int cur_end_lane = getLogValue(END_LANE,0);
	//check all the lanes...
	if(lane_danger_map.count(cur_end_lane) != 0)
	{
	  int cur_lane_danger = lane_danger_map[cur_end_lane];
	  int target_lane = cur_end_lane;
	  int target_danger = cur_lane_danger;
	  // 	  std::cout 
	  //  	      << "cur_lane_danger" << cur_lane_danger
	  // 	      << std::endl;
	  if(cur_end_lane > 0)
	  {
	    //can turn left
	    int danger = 0;
	    if(lane_danger_map.count(cur_end_lane - 1) != 0)
	    {
	      danger = lane_danger_map[cur_end_lane - 1];
	      
	    }
	    if(danger < cur_lane_danger)
	    {
	      target_lane = cur_end_lane - 1;
	      target_danger = danger;
	    }
	  }
	  
	  if(cur_end_lane < TRACK_INFO["lanes"].size() - 1)
	  {
	    //can turn left
	    int danger = 0;
	    if(lane_danger_map.count(cur_end_lane + 1) != 0)
	    {
	      int danger = lane_danger_map[cur_end_lane + 1];
	    }
	    if(danger < cur_lane_danger)
	    {
	      target_lane = cur_end_lane + 1;
	      target_danger = danger;
	    }
	  }
	  retVal = target_lane - cur_end_lane;
	  
	  if(retVal != 0)
	  {
// 	   	  std::cout 
// 	    	      << "Overtake to " << retVal
// 	   	      << std::endl;
	  }
	}
	
      }
      return retVal;
    }
    
    bool useTurbo()
    {
        bool retVal = false;
        
        if(DIST_ACC_MAP.size() != 0 && 
	  ACC_DIST_MAP.size() != 0 && 
	  USED_TURBO_FACTOR > 0.99 && 
	  USED_TURBO_FACTOR < 1.01 && 
	  TURBO_READY)
        {
            
//             std::cout << CURR_ACC_DIST << "---" << CURR_ACC_COUNT << std::endl;
            
//             std::cout << "-Can perhaps turbo?--" << std::endl;
            double curr_dist = getLogValue(PIECE_INDEX,0)*1000 + getLogValue(PIECE_DIST,0);
            
            std::map<double,int>::iterator it_dist;
            
            it_dist = DIST_ACC_MAP.lower_bound (curr_dist);
            if(it_dist == DIST_ACC_MAP.end())
            {
                it_dist--;
            }

            std::map<int,double>::reverse_iterator it_acc = ACC_DIST_MAP.rbegin();
          
            int max_acc = ACC_DIST_MAP[it_dist->second];
            
            double distDiff = curr_dist - it_acc->second;
            
//              std::cout << "-Dist....: " << distDiff << std::endl;
            if(distDiff < 0)
            {
                distDiff = distDiff + TRACK_INFO["pieces"].size() * 1000;
            }
            
            if(distDiff < 1000)
            {
                std::cout << "-Yess....: " << distDiff << std::endl;
                         std::cout << "Angle: " << getLogValue(SLIP_ANGLE, 0) 
         << ", Piece: " << getLogValue(PIECE_INDEX, 0)
         << ", Dist: " << getLogValue(PIECE_DIST, 0)
//         << ", Speed: " << getLogValue(SPEED, 0) 
//         << ", Throttle: " << (RACE_LOG.size() > 0 ? RACE_LOG.at(RACE_LOG.size() - 1)[THROTTLE] : -1)
         << ", Radius: " << getPieceRadius( getLogValue(PIECE_INDEX, 0), getLogValue(START_LANE, 0))
//         << ", Start lane: " << getLogValue(START_LANE, 0)
         << std::endl;
                retVal = true;
            }
            else
            {
//                 std::cout << "-Not... to far from optimal location--" << distDiff << std::endl;
            }
            
//             distDiff = distDiff / 1000;
//             
//             if(max_acc == ACC_DIST_MAP.rbegin()->first && distDiff(max_acc - distDiff*10) / sqrt(TURBO_FACTOR) > TURBO_TICKS)
//             {
//                 retVal = true;
//             }
        }
        
        return retVal;
    }
    
    void debugPrintOut()
    {
//         std::cout << "Angle: " << getLogValue(SLIP_ANGLE, 0) 
//         << ", Piece: " << getLogValue(PIECE_INDEX, 0)
//         << ", Dist: " << getLogValue(PIECE_DIST, 0)
//         << ", Speed: " << getLogValue(SPEED, 0) 
//         << ", Throttle: " << (RACE_LOG.size() > 0 ? RACE_LOG.at(RACE_LOG.size() - 1)[THROTTLE] : -1)
//         << ", Radius: " << getPieceRadius( getLogValue(PIECE_INDEX, 0), getLogValue(START_LANE, 0))
//         << ", Start lane: " << getLogValue(START_LANE, 0)
//         << std::endl;
    }
    
}

game_logic::game_logic()
: action_map
{
    { "join", &game_logic::on_join },
    { "gameInit", &game_logic::on_game_init },
    { "gameStart", &game_logic::on_game_start },
    { "carPositions", &game_logic::on_car_positions },
    { "yourCar", &game_logic::on_your_car },
    { "turboAvailable", &game_logic::on_turbo_available },
    { "turboStart", &game_logic::on_turbo_start },
    { "turboEnd", &game_logic::on_turbo_end },
    { "crash", &game_logic::on_crash },
    { "spawn", &game_logic::on_spawn },
    { "finish", &game_logic::on_game_end },
    { "lapFinished", &game_logic::on_lap_finish },
    { "error", &game_logic::on_error}
},
last_distance(0.0),
last_speed(0.0),
last_angle(0.0),
req_switch(false),
max_angle(0),
min_angle(0)
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
    const auto& msg_type = msg["msgType"].as<std::string>();
    auto action_it = action_map.find(msg_type);
    if (action_it != action_map.end())
    {
        return (action_it->second)(this, msg);
    }
    else
    {
        std::cout << "Unknown message type: " << msg_type << std::endl;
        return { make_request("skip", "")};
    }
    
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& msg)
{
    std::cout << "Joined" << std::endl;

    
        return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& msg)
{
//     std::cout << "Init" << msg.to_string() << std::endl;
    track_info = msg["data"]["race"]["track"];
    TRACK_INFO = msg["data"]["race"]["track"]; 
    
    const auto& pieces = msg["data"]["race"]["track"]["pieces"];
    
    int switchIndex = -1;
    int firstSwitchIndex = -1;
    int piece_count = pieces.size();
    int current_sum = 0;
    for(int i=0; i < piece_count; ++i)
    {
        
        const auto& piece = pieces[i];
        if(piece.get("switch",false).as<bool>())
        {
            firstSwitchIndex = i;
            switchIndex = i;
            break;
        }
    }
    
    for(int i=1; i < piece_count+1; ++i)
    {
        int actualIndex = firstSwitchIndex + i;
        actualIndex = actualIndex < piece_count ? actualIndex : actualIndex - piece_count;
        const auto& piece = pieces[actualIndex];
        
        if(piece.get("switch",false).as<bool>())
        {
            if(current_sum != 0)
            {
                m_switch_map.insert(std::pair<int,int>(switchIndex, current_sum));
                SWITCH_MAP.insert(std::pair<int,int>(switchIndex, current_sum));
            }
            current_sum = 0;
            switchIndex = actualIndex;
        }
        else
        {
            current_sum = current_sum + piece.get("radius",0).as<int>() * piece.get("angle",0.0).as<double>();
        }
    }
    
    //clear only for qualifying session
    if(msg["data"]["race"]["raceSession"].get("durationMs",0).as<int>() != 0 ||
       msg["data"]["race"]["raceSession"].get("quickRace",false).as<bool>()
    )
    {
        LAST_CORNER_SPEED = 0;
        LAST_CORNER_RADIUS = 0;
        
        BASE_RADIUS = 0;
        BASE_SPEED = 0;
        
        ENGINE_POWER = 0;
        ACCELERATION_COEFF = 0;
        SLIDE_TORQUE = 0;
        MAX_SLIDE = 8.0;
        
        TRACK_MAX_INVALID = true;
        
        REQ_SWITCH = false;
        
        RACE_LOG.clear();
        CURRENT_TICK_LOG.clear();
        
        ACC_DIST_MAP.clear();
        DIST_ACC_MAP.clear();
	
	LAP_TIMES.clear();
    }
    
    
        return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& msg)
{
    std::cout << "Race started" << std::endl;


    

    TRACKING_ACC = false;
    TURBO_READY = false;
    TURBO_FACTOR = 1;
    USED_TURBO_FACTOR = 1.0;
    TURBO_IN_USE = false;
    TURBO_TICKS = 0;
    
    CURR_ACC_DIST = 0;
    CURR_ACC_COUNT = 0;
    
    TRACK_MAX_ANGLE = 0;
    return { make_ping() };
}


game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& msg)
{
    const auto& data = msg["data"];
    CAR_COLOR =  data["color"].as<std::string>();
        return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& msg)
{
    startNewTick(msg);
    
    double throttle = 1.0;
    
    if(ENGINE_POWER == 0)
    {
        checkEnginePower();
        throttle = 1.0;
    } 
    else if(SLIDE_TORQUE == 0)
    {
        throttle = checkCornerSpeed();
    }
    else
    {
      //do not change the direction
        throttle = getAdjustedSpeed(false, 0);
        
        //We can start tracking acceleration only if the car reached max corner speed and starts to slow down
        if(throttle < 0.01 && !TRACKING_ACC)
        {
            TRACKING_ACC = true;
        }
        if( throttle > 0.99 && 
            getLogValue(SPEED,0) > RACE_LOG.at(RACE_LOG.size()-1)[SPEED] &&
            (fabs(getLogValue(SLIP_ANGLE, 0)) < 30 || getPieceRadius( getLogValue(PIECE_INDEX, 0), getLogValue(END_LANE, 0)) == 0) &&
            getLogValue(SPEED,0) > 4
            )
        {
            if(CURR_ACC_COUNT == 0)
            {
                //assuming that the piece length will never be longer that 1000 :)
                CURR_ACC_DIST = getLogValue(PIECE_INDEX,0)*1000 + getLogValue(PIECE_DIST,0);
            }
            CURR_ACC_COUNT++;
        }
        else
        {
            if(CURR_ACC_COUNT > 0)
            {
                ACC_DIST_MAP.insert(std::pair<int,double>(CURR_ACC_COUNT, CURR_ACC_DIST));
                DIST_ACC_MAP.insert(std::pair<double,int>(CURR_ACC_DIST, CURR_ACC_COUNT));
            }
            CURR_ACC_COUNT = 0;
        }
        
        if(safeToNotThrottle(throttle))
        {
// 	  std::cout << "Safe to throttle!!!!--" << std::endl;
            if(useTurbo())
            {
                setLogValue(THROTTLE, throttle);
                debugPrintOut();
                TURBO_READY = false;
//                 std::cout << "-Turbo!!!!--" << std::endl;
                return { make_turbo("Speed of light!!")};
            }
            
            bool overtakeDirection = overtake();
            
            if(overtakeDirection < 0 && getAdjustedSpeed(true, -1) != 0)
	    {
                setLogValue(THROTTLE, throttle);
                debugPrintOut();
                return { make_request("switchLane", "Left")};
	    }
	    else if(overtakeDirection > 0 && getAdjustedSpeed(true, 1) != 0)
	    {
	      
                setLogValue(THROTTLE, throttle);
                debugPrintOut();
                return { make_request("switchLane", "Right")};
	    }
            
            int start_lane = getLogValue(START_LANE,0);
            int end_lane = getLogValue(END_LANE,0);
            int piece_index = getLogValue(PIECE_INDEX,0);
            int switchLane = switchLanes(piece_index, start_lane, end_lane);
            
            //////////////////SWITCHING LANE//////////////////////////////////
            //Ths basically check if the last throttle has bee correctly recognized by the server
            //and that the current throttle doesnt differ from previous one
            
            if(switchLane < 0)
            {
                //             std::cout << "switching" << std::endl;
                setLogValue(THROTTLE, throttle);
                debugPrintOut();
                return { make_request("switchLane", "Left")};
            }
            else if(switchLane > 0)
            {
                //             std::cout << "switching" << std::endl;
                setLogValue(THROTTLE, throttle);
                debugPrintOut();
                return { make_request("switchLane", "Right")};
            }
        }
        else
	{
	  
// 	  std::cout << "UnSafe to throttle!!!!--" << std::endl;
	}
    }
    
    setLogValue(THROTTLE, throttle);
    
    debugPrintOut();
    
    return { make_throttle(throttle) };
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& msg)
{
    const auto& data = msg["data"];
    TURBO_READY = true;
    TURBO_TICKS = data["turboDurationTicks"].as<int>();   
    TURBO_FACTOR = data["turboFactor"].as<double>();
//     std::cout << "turboAvailable: " 
//     << ", TURBO_TICKS: " << TURBO_TICKS
//     << ", TURBO_FACTOR: " << TURBO_FACTOR
//     << std::endl;
    
        return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_turbo_start(const jsoncons::json& msg)
{
  const auto& data = msg["data"];
  const auto& car_color = data["color"].as<std::string>();
  if(car_color.compare(CAR_COLOR) == 0)
  {
    USED_TURBO_FACTOR = TURBO_FACTOR;
  }
  return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_turbo_end(const jsoncons::json& msg)
{
  const auto& data = msg["data"];
  const auto& car_color = data["color"].as<std::string>();
  if(car_color.compare(CAR_COLOR) == 0)
  {
    USED_TURBO_FACTOR = 1.0;
  }
  return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& msg)
{
     
    const auto& data = msg["data"];
    const auto& car_color = data["color"].as<std::string>();
    if(car_color.compare(CAR_COLOR) == 0)
    {
        TRACK_MAX_INVALID = true;
        std::cout << "Crashed" << std::endl;
        
//         std::cout << "Angle: " << getLogValue(SLIP_ANGLE, 0) 
//         << ", Piece: " << getLogValue(PIECE_INDEX, 0)
//         << ", Dist: " << getLogValue(PIECE_DIST, 0)
//         << ", Speed: " << getLogValue(SPEED, 0) 
//         << ", Throttle: " << (RACE_LOG.size() > 0 ? RACE_LOG.at(RACE_LOG.size() - 1)[THROTTLE] : -1)
//         << ", Radius: " << getPieceRadius( getLogValue(PIECE_INDEX, 0), getLogValue(END_LANE, 0))
//         << std::endl;
    }
    
    
        return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& msg)
{
    const auto& data = msg["data"];
    
    const auto& car_color = data["color"].as<std::string>();
    if(car_color.compare(CAR_COLOR) == 0)
    {
        std::cout << "spawned" << std::endl;
        return { make_throttle(1.0) };
    }
        return { make_request("skip", "")};
}


game_logic::msg_vector game_logic::on_lap_finish(const jsoncons::json& msg)
{

    
    const auto& data = msg["data"];
    const auto& car_color = data["car"]["color"].as<std::string>();
    if(car_color.compare(CAR_COLOR) == 0)
    {
        
        if(TRACK_MAX_ANGLE < 58 && !TRACK_MAX_INVALID)
        {
            MAX_SLIDE = MAX_SLIDE + (59-TRACK_MAX_ANGLE)/10;
            std::cout << "TRACK_MAX_ANGLE: " << TRACK_MAX_ANGLE << ", increase MAX_SLIDE to: " << MAX_SLIDE << std::endl;
        }
        
        TRACK_MAX_ANGLE = 0;
        TRACK_MAX_INVALID = false;
        std::cout << "Lap Time: " 
        << data["lapTime"]["millis"].as<int>() << " ms,  " 
        << data["lapTime"]["ticks"].as<int>() << " ticks"
        << std::endl;
    }
    
    //Save the lap time of that car
    int cur_lap = data["lapTime"]["ticks"].as<int>();
    
    if(LAP_TIMES.count(car_color) == 0)
    {
      LAP_TIMES.insert(std::pair<std::string, int>(car_color, cur_lap));
    }
    else
    {
      if(cur_lap < LAP_TIMES[car_color])
      {
	 LAP_TIMES.insert(std::pair<std::string, int>(car_color, cur_lap));
      }
    }
    
    return { make_request("skip", "")};
    
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& msg)
{
    std::cout << "Race ended" << std::endl;
    std::cout << "min_angle" << min_angle << std::endl;
    std::cout << "max_angle" << max_angle << std::endl;
        return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& msg)
{
    std::cout << "Error: " << msg.to_string() << std::endl;
        return { make_request("skip", "")};
}
