#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>

class game_logic
{
public:
    typedef std::vector<jsoncons::json> msg_vector;
    
    game_logic();
    msg_vector react(const jsoncons::json& msg);
    
private:
    typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
    const std::map<std::string, action_fun> action_map;
    double last_distance;
    double last_speed;
    double last_angle;
    jsoncons::json   track_info;
    bool req_switch;
    double max_angle;
    double min_angle;
    std::map<int, int> m_switch_map; //piece index, sum of angle*radius before next switch
    
    msg_vector on_join(const jsoncons::json& msg);
    msg_vector on_game_init(const jsoncons::json& msg);
    msg_vector on_game_start(const jsoncons::json& msg);
    msg_vector on_your_car(const jsoncons::json& msg);
    msg_vector on_car_positions(const jsoncons::json& msg);
    msg_vector on_turbo_available(const jsoncons::json& msg);
    msg_vector on_turbo_start(const jsoncons::json& msg);
    msg_vector on_turbo_end(const jsoncons::json& msg);
    msg_vector on_crash(const jsoncons::json& msg);
    msg_vector on_spawn(const jsoncons::json& msg);
    msg_vector on_game_end(const jsoncons::json& msg);
    msg_vector on_lap_finish(const jsoncons::json& msg);
    msg_vector on_error(const jsoncons::json& msg);
    
};

#endif
