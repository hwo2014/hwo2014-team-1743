#ifndef CarsInfo_h
#define CarsInfo_h

#include "TrackInfo.h"
#include <vector>
#include <string>
#include <map>

class CarsInfo {
public:
    CarsInfo(const TrackInfo &track): m_track(track) {}

    class Car {
    public:
        Car(): piece(0), lap(0), startLane(0), endLane(0)
        ,angle(0), angleSpeed(0), angleAcc(0)
        ,inPieceDist(0), speed(0), acc(0), speedAverage(0) {}

        Car(const Car &o): piece(o.piece), lap(o.lap), startLane(o.startLane), endLane(o.endLane)
        ,angle(o.angle), angleSpeed(o.angleSpeed), angleAcc(o.angleAcc)
        ,inPieceDist(o.inPieceDist), speed(o.speed), acc(o.acc), speedAverage(o.speedAverage) {}

        const Car &operator=(const Car &o) {
            if (&o == this) return *this;
            name = o.name;
            color = o.color;
            piece = o.piece;
            lap = o.lap;
            startLane = o.startLane;
            endLane = o.endLane;
            angle = o.angle;
            angleSpeed = o.angleSpeed;
            angleAcc = o.angleAcc;
            inPieceDist = o.inPieceDist;
            speed = o.speed;
            acc = o.acc;
            speedAverage = o.speedAverage;

            return *this;
        }

        std::string name;
        std::string color;
        size_t piece;
        size_t lap;
        size_t startLane;
        size_t endLane;
        double angle;
        double angleSpeed;
        double angleAcc;
        double inPieceDist;
        double speed;
        double acc;
        double speedAverage;
    };

    void updateCars(const jsoncons::json &data);

    std::map<std::string, Car> cars;
    // FIXME add car size here for calculation for bigger cars

    double distToCar(const std::string &fromColor, const std::string &toColor);
    double distToCar(const CarsInfo::Car &carFrom, const CarsInfo::Car &carTo);
    double averageSpeedDiff(const std::string &fromColor, const std::string &toColor);

private:
    const TrackInfo &m_track;
};

#endif

