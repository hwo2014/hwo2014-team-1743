#ifndef HWO_GAME_LOGIC_H
#define HWO_GAME_LOGIC_H

#include "CarsInfo.h"
#include "TrackInfo.h"
#include <string>
#include <vector>
#include <map>
#include <functional>
#include <iostream>
#include <jsoncons/json.hpp>

class game_logic
{
public:
  typedef std::vector<jsoncons::json> msg_vector;

  game_logic();
  msg_vector react(const jsoncons::json& msg);

private:
  typedef std::function<msg_vector(game_logic*, const jsoncons::json&)> action_fun;
  const std::map<std::string, action_fun> action_map;

  msg_vector on_join(const jsoncons::json& data);
  msg_vector on_game_start(const jsoncons::json& data);
  msg_vector on_game_end(const jsoncons::json& data);
  msg_vector on_car_positions(const jsoncons::json& data);
  msg_vector on_crash(const jsoncons::json& data);
  msg_vector on_spawn(const jsoncons::json& data);
  msg_vector on_error(const jsoncons::json& data);
  msg_vector on_your_car(const jsoncons::json& data);
  msg_vector on_game_init(const jsoncons::json& data);

  msg_vector on_turbo_available(const jsoncons::json& msg);
  msg_vector on_turbo_start(const jsoncons::json& msg);
  msg_vector on_turbo_end(const jsoncons::json& msg);
  msg_vector on_lap_finish(const jsoncons::json& msg);
  
  void findBestTurboPosition(const CarsInfo::Car &ccar);
  bool useTurbo_v2();
  void trackThrottle();

  double simulateMaxAngle(const CarsInfo::Car &ccar, int switchLane, bool &switchOnCurve, double &crashDist, const std::vector<double> & throttleList);
  double newMaximalThrottle(const CarsInfo::Car &car, int switchLane, bool &switchOnCurve, double customTurbo = 0, double turboDuration = 1);
  double turboThrottle(const CarsInfo::Car &ccar, int switchLane, bool &switchOnCurve);
  CarsInfo::Car simulateStep(const CarsInfo::Car &ccar, double throttle, int switchLane, double customTurbo = 0);
  void adjustParameters_v2();
  void adjustParameters();
  void simulateParameters(double &c_vAMax, double &c_aMax, double &accDiff, double &angleDiff);

  int followTarget();
  bool canBeEvil(bool &warning);
  bool carAhead();
  bool carBehind();
  int overtake(bool &overtaking);
  int goSafeLane(bool &safeLaning);

  TrackInfo m_tInfo;
  CarsInfo m_cInfo;

  std::string m_color;

  double m_defTorque; // The default normal acceleration to exceed to change the angle.
  double m_torque; // The normal acceleration to exceed to change the angle.
  double m_torqueMin; // The normal acceleration to exceed to change the angle.
  double m_multiplier; // A magic multiplier that converts the forces to angles.
  double m_damping; // The damping multiplier that stops the oscillations.
  double m_springConst; // The spring constant that forces the angle back.
  double m_maxSpeed; // The maximum speed.
  double m_accCoeff; // The acceleration coefficient for acceleration/breaking.
  double m_v1; // The first speed of the game > 0.
  int    m_turboCounter;  // The ticks remaining with turbo.
  double m_turboMultip; // The active turbo multiplier.
  double m_turboMultipAvailable; // The available turbo multiplier.
  int    m_turboAvailable; // shows how many ticks turbo is available.
  int    m_switchLane;

  double m_targetSpeed; // Target speed for ..... calculating torque,...
  double m_throttle; // Last sent throttle

  bool m_optimize; // should we optimize the parameters
  bool m_crash; // we have probably crashed
  
  int m_bumpTick; //The last tick on which the car was bumpped
  int m_gameTick; //true if the car crashed after a bump
  bool m_bumpCrash; //true if the car crashed after a bump
  bool m_optimizeWhenCrashed; //it is true if the car crashed during optimization phase
  
  double m_maxAngleLimit;
  double m_currentLimit;
  double m_maxAngleDriven;
  
  //turbo stuff
  bool m_trackThrottle;
  int m_throttleCount;
  double m_throttleDist;
  std::map<int, double> m_throttle_dist_map;
  std::map<double, int> m_dist_throttle_map;
  
  //evil stuff
  std::string m_lockedTarget;

  CarsInfo::Car m_simCar;
  CarsInfo::Car m_lastCar;

  std::vector<CarsInfo::Car> m_cars;
  
  std::map<double, double> m_defaultSpeed; //contains simulated speed of the car on the whole lap <dist, speed>

};

#endif
