#include "game_logic.h"
#include "protocol.h"
#include <math.h>
#include <algorithm>    // std::max

#include <boost/timer.hpp>


using namespace hwo_protocol;

game_logic::game_logic()
: action_map
{
    { "join", &game_logic::on_join },
    { "gameStart", &game_logic::on_game_start },
    { "gameEnd", &game_logic::on_game_end },
    { "carPositions", &game_logic::on_car_positions },
    { "crash", &game_logic::on_crash },
    { "spawn", &game_logic::on_spawn },
    { "error", &game_logic::on_error },
    { "yourCar", &game_logic::on_your_car },
    { "turboAvailable", &game_logic::on_turbo_available },
    { "turboStart", &game_logic::on_turbo_start },
    { "turboEnd", &game_logic::on_turbo_end },
    { "lapFinished", &game_logic::on_lap_finish },
    { "gameInit", &game_logic::on_game_init }
}
,m_cInfo(m_tInfo)
,m_defTorque(0.25)
,m_torque(0)
,m_torqueMin(0)
,m_multiplier(0.53)
,m_damping(0.9) // assume constant
,m_springConst(0.00125)
,m_maxSpeed(0) // check the maxSpeed in the beginning
,m_accCoeff(0) // check the maxSpeed in the beginning
,m_v1(0)
,m_turboCounter(0)
,m_turboMultip(1)
,m_turboMultipAvailable(1)
,m_turboAvailable(0)
,m_switchLane(0)
,m_throttle(1.0)
,m_optimize(true)
,m_crash(false)
,m_bumpTick(-1)
,m_gameTick(-1)
,m_bumpCrash(false)
,m_optimizeWhenCrashed(false)
,m_maxAngleLimit(60)
,m_maxAngleDriven(0)
,m_trackThrottle(false)
,m_throttleCount(0)
,m_throttleDist(0.0)
{
}

game_logic::msg_vector game_logic::react(const jsoncons::json& msg)
{
    const auto& msg_type = msg["msgType"].as<std::string>();
    auto action_it = action_map.find(msg_type);
    if (action_it != action_map.end())
    {
        return (action_it->second)(this, msg);
    }
    else
    {
        std::cout << "Unknown message type: " << msg_type << std::endl;
        return { make_request("skip", "")};
    }
}

game_logic::msg_vector game_logic::on_join(const jsoncons::json& msg)
{
    std::cout << "Joined" << std::endl;
    return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_game_start(const jsoncons::json& msg)
{
    std::cout << "Race started" << std::endl;
    return { make_ping() };
}

game_logic::msg_vector game_logic::on_game_end(const jsoncons::json& msg)
{
    std::cout << "gameEnd" << std::endl;
    m_simCar = CarsInfo::Car();
    m_lastCar = CarsInfo::Car();
    m_cInfo.cars.clear();
    m_cars.clear();
    
    m_throttle = 1;
    m_crash = false;
    m_turboCounter = 0;
    m_turboMultip = 1;
    m_turboMultipAvailable = 1;
    m_turboAvailable = 0;
    m_bumpTick = -1;
    m_gameTick = -1;
    m_bumpCrash = false;
    m_optimizeWhenCrashed = false;
    m_trackThrottle = false;
    m_throttleCount = 0;
    m_throttleDist = 0.0;
    
    return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_your_car(const jsoncons::json& msg)
{
    const auto& data = msg["data"];
    m_color = data["color"].as<std::string>();
    //std::cout << "yourCar color: " << m_color << std::endl;
    return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_game_init(const jsoncons::json& msg)
{
    const auto& data = msg["data"];
    const auto &race = data["race"];
    
    // Race Session
    
    // Track
    const auto &track = race["track"];
    m_tInfo.id = track["id"].as<std::string>();
    m_tInfo.name = track["name"].as<std::string>();
    
    // Lanes
    const auto &lanes = track["lanes"];
    m_tInfo.laneCenterOffset.resize(lanes.size());
    for (size_t i=0; i<lanes.size(); i++) {
        size_t index = lanes[i]["index"].as<int>();
        if (index < 0 || index > lanes.size()) {
            std::cout << "list index out of range" << std::endl;
            continue;
        }
        double dist = lanes[i]["distanceFromCenter"].as<double>();
        m_tInfo.laneCenterOffset[index] = dist;
    }
    
    // pieces
    m_tInfo.clearTrack();
    const auto &pieces = track["pieces"];
    for (size_t i=0; i<pieces.size(); i++) {
        m_tInfo.addPiece(pieces[i]);
    }
    
    m_tInfo.calculateTurbo();
    
    return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_crash(const jsoncons::json& msg)
{
    const auto& data = msg["data"];
    const auto& car_color = data["color"].as<std::string>();
    if(car_color.compare(m_color) == 0)
    {
        //has been bumpped?
        if(m_gameTick - m_bumpTick < 30)
        {
            m_bumpCrash = true;
        }
        
        std::cout << "Bumpped here: " << m_bumpTick << std::endl;
        std::cout << "Crashed here: " << m_gameTick << std::endl;
        
        m_turboMultip = 1.0;
        m_turboAvailable = 0;
        m_trackThrottle = false;
        
        std::cout << "Crashed" << std::endl;
        
        std::cout << "Angle: " << m_cInfo.cars[m_color].angle
        << ", Piece: " << m_cInfo.cars[m_color].piece
        << ", Dist: " << m_cInfo.cars[m_color].inPieceDist
        << ", Speed: " << m_cInfo.cars[m_color].speed
        << ", Throttle: " << m_throttle
        << ", Radius: " << m_tInfo.radius(m_cInfo.cars[m_color].piece, m_cInfo.cars[m_color].inPieceDist, m_cInfo.cars[m_color].startLane, m_cInfo.cars[m_color].endLane)
        << std::endl;
    }
    return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_spawn(const jsoncons::json& msg)
{
    const auto& data = msg["data"];
    const auto& car_color = data["color"].as<std::string>();
    if(car_color.compare(m_color) == 0)
    {
        std::cout << "Spawn" << std::endl;
        m_turboMultip = 1.0;
        m_turboAvailable = 0;
        m_turboMultipAvailable = 1.0;
        m_trackThrottle = false;
    }
    return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_error(const jsoncons::json& msg)
{
    const auto& data = msg["data"];
    std::cout << "Error: " << data.to_string() << std::endl;
    return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_turbo_available(const jsoncons::json& msg)
{
    //std::cout << msg.as<std::string>() << std::endl;
    const auto& data = msg["data"];
    m_turboAvailable = data["turboDurationTicks"].as<int>();
    m_turboMultipAvailable = data["turboFactor"].as<double>();
    //std::cout << "turboAvailable: "
    //<< ", ticks: " << m_turboAvailable
    //<< ", factor: " << m_turboMultipAvailable
    //<< std::endl;
    
    return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_turbo_start(const jsoncons::json& msg)
{
    const auto& data = msg["data"];
    const auto& car_color = data["color"].as<std::string>();
    if(car_color.compare(m_color) == 0) {
        std::cout << msg.as<std::string>() << std::endl;
        m_turboCounter = m_turboAvailable;
        m_turboMultip = m_turboMultipAvailable;
        m_turboAvailable = 0;
    }
    return { make_request("skip", "")};
}

game_logic::msg_vector game_logic::on_turbo_end(const jsoncons::json& msg)
{
    const auto& data = msg["data"];
    const auto& car_color = data["color"].as<std::string>();
    if(car_color.compare(m_color) == 0) {
        //std::cout << msg.as<std::string>() << std::endl;
        m_turboMultip = 1.0;
    }
    return { make_request("skip", "")};
}


game_logic::msg_vector game_logic::on_lap_finish(const jsoncons::json& msg)
{
    const auto& data = msg["data"];
    const auto& car_color = data["car"]["color"].as<std::string>();
    if(car_color.compare(m_color) == 0) {
        std::cout << "Lap Time: "
        << data["lapTime"]["millis"].as<int>() << " ms,  "
        << data["lapTime"]["ticks"].as<int>() << " ticks"
        << std::endl;
        
        std::cout << "Max Lap Angle: " << m_maxAngleDriven << std::endl;
        m_maxAngleDriven = 0;
    }
    return { make_request("skip", "")};
}


game_logic::msg_vector game_logic::on_car_positions(const jsoncons::json& msg)
{    
    const auto& data = msg["data"];
    int gameTick = msg.get("gameTick", -1).as<int>();
    m_gameTick = gameTick;
    if(m_cInfo.cars.count(m_color) > 0)
    {
        m_lastCar = m_cInfo.cars[m_color];
    }
    m_cInfo.updateCars(data);
    const CarsInfo::Car &car = m_cInfo.cars[m_color];
    
    m_maxAngleDriven = std::max(m_maxAngleDriven, std::fabs(car.angle));
    
    if (gameTick == -1) {
        return { make_request("skip", "")};
    }
    double currentRadius = m_tInfo.radius(car.piece, car.inPieceDist, car.startLane, car.endLane);
    //     std::cout << "Tick: " << gameTick << std::endl;
    
    // *** Calculate the acceleration coefficients and max speed ***
    if (m_v1 == 0) {
        //std::cout << car.speed << " " << car.inPieceDist << " " << gameTick << std::endl;
        if (car.speed == 0) {
            // this is the first car position message.
            return { make_throttle(m_throttle) };
        }
        // This is the second position message
        m_v1 = car.speed;
        return { make_throttle(m_throttle) };
    }
    
    if (m_accCoeff == 0) {
        // This is the third position message
        m_accCoeff = m_v1 / (m_v1*2 - car.speed);
        m_maxSpeed = m_v1 * m_accCoeff;
        std::cout << "acc, max = " << m_accCoeff << ", " << m_maxSpeed << " " << m_v1 << " " << car.speed << std::endl;
    }
    
    if (m_optimize) {
        if(m_cars.size() <= 100)
        {
            if( m_cars.size() != 0 ||
                (m_cars.size() == 0 && std::fabs(currentRadius) < 1000)
            )
            {
                m_cars.push_back(car);
            }
        }
        
        if (m_cars.size() > 5 && m_torque != 0) {
            // simulate the
            adjustParameters();
        }
    }
    
    // *** calculate the torque (slip/slide speed limit -> normal acceleration limit) ***
    if (m_torque == 0) {
        // std::cout << car.speed << " " << car.angle << " " << car.angleSpeed << " " << m_tInfo.radius(car.piece, car.startLane, car.endLane) << " 1" << std::endl;
        if (std::fabs(car.angle) > 0.0) {
            // we have exceeded the slide limit
            m_torque = m_torqueMin + 0.005;
            m_throttle = 1.0;
            m_simCar = car;
            std::cout << " Torque: " << m_torque << " " << m_torqueMin << std::endl;
            return { make_throttle(m_throttle) };
        }
        
        
        
        if(currentRadius < 100000)
        {
            m_torqueMin = std::fabs((car.speed * car.speed) / m_tInfo.radius(car.piece, car.inPieceDist, car.startLane, car.endLane));
            // std::cout << " Torque min: " << m_torqueMin << " " << m_torqueMin << std::endl;
        }
        
        
        // get next curve piece and calculate the target speed
        size_t firstCornerPiece = car.piece + 1;
        if (firstCornerPiece >= m_tInfo.pieceCount()) firstCornerPiece = 0;
        
        while (m_tInfo.radius(firstCornerPiece, car.inPieceDist, car.startLane, car.endLane) > 100000 || 
            (m_tInfo.radius(firstCornerPiece, car.inPieceDist, car.startLane, car.endLane) == currentRadius && firstCornerPiece != car.piece)
        ) 
        {
            firstCornerPiece++;
            if (firstCornerPiece >= m_tInfo.pieceCount()) firstCornerPiece = 0;
        }
        double firstCornerRadius = m_tInfo.radius(firstCornerPiece, car.inPieceDist, car.startLane, car.endLane);
        
        size_t secondCornerPiece = firstCornerPiece + 1;
        if (secondCornerPiece >= m_tInfo.pieceCount()) secondCornerPiece = 0;
        
        while (m_tInfo.radius(secondCornerPiece, car.inPieceDist, car.startLane, car.endLane) > 100000 || 
            (m_tInfo.radius(secondCornerPiece, car.inPieceDist, car.startLane, car.endLane) == firstCornerRadius && secondCornerPiece != firstCornerPiece) 
        )
        {
            secondCornerPiece++;
            if (secondCornerPiece >= m_tInfo.pieceCount()) secondCornerPiece = 0;
        }
        double secondCornerRadius = m_tInfo.radius(secondCornerPiece, car.inPieceDist, car.startLane, car.endLane);;
        
        double firstCornerSpeed  = std::sqrt(std::max(m_defTorque, m_torqueMin) * std::fabs(firstCornerRadius));
        double secondCornerSpeed = std::sqrt(std::max(m_defTorque, m_torqueMin) * std::fabs(secondCornerRadius));
        double firstCornerDist = m_tInfo.distToPiece(car.piece, car.inPieceDist, car.startLane, car.endLane, firstCornerPiece);
        double secondCornerDist = m_tInfo.distToPiece(car.piece, car.inPieceDist, car.startLane, car.endLane, secondCornerPiece);
        
        // std::cout << "1st corner piece: " << firstCornerPiece << ", 2nd corner piece: " << secondCornerPiece << std::endl;
        // std::cout << "1st corner dist: " << firstCornerDist << ", 2nd corner dist: " << secondCornerDist << std::endl;
        
        //check if we the speed os second corner is so low, that we need to brake before first cornet
        if(car.speed > secondCornerSpeed &&
            (car.speed - secondCornerSpeed) * m_accCoeff > secondCornerDist
        )
        {
            //need to start slowing down
            m_targetSpeed = secondCornerSpeed;
            // std::cout << "second corner: " << m_targetSpeed << std::endl;
        }
        //check if we need to brake before the next corner
        else if(car.speed > firstCornerSpeed &&
            (car.speed - firstCornerSpeed) * m_accCoeff > firstCornerDist
        )
        {
            //need to start slowing down
            m_targetSpeed = firstCornerSpeed;
            // std::cout << "first corner: " << m_targetSpeed << std::endl;
        }
        else
        {
            if(currentRadius < 100000)
            {
                m_targetSpeed = 100;
            }
            else
            {
                m_targetSpeed = firstCornerSpeed;
            }
        }
        double throttle = 0;
        throttle = m_targetSpeed/m_maxSpeed;
        throttle = throttle < 0 ? 0.0 : (throttle >= 1 ? 1.0 : throttle);
        
        if(m_throttle == throttle)
        {
            int switchLaneDir = m_tInfo.switchLane(car.piece, car.endLane);
            if(switchLaneDir != 0)
            {
                // switching to optimal lane
                if (switchLaneDir < 0) {
                    m_switchLane = -1;
                    //std::cout << "Switch lane Left " << car.piece << " " << m_tInfo.radius(car.piece, car.startLane, car.endLane) << std::endl;
                    return { make_request("switchLane", "Left")};
                }
                if (switchLaneDir > 0) {
                    m_switchLane = 1;
                    //std::cout << "Switch lane Right " << car.piece << " " << m_tInfo.radius(car.piece, car.startLane, car.endLane) << std::endl;
                    return { make_request("switchLane", "Right")};
                }
            }
        }
        else
        {
            m_throttle = throttle;
        }
        
        return { make_throttle(m_throttle) };
        
        // m_throttle = (car.speed < m_targetSpeed) ? 1 : m_targetSpeed/m_maxSpeed;
        // std::cout << "target speed: " << m_targetSpeed << " " << car.piece  << " " << m_tInfo.radius(car.piece, car.startLane, car.endLane)<< std::endl;
        // std::cout << car.speed << " " << car.angle << " " << m_throttle << std::endl;
        
    }
    
    // Calculate max throttle
    if(m_lastCar.startLane == m_lastCar.endLane && car.startLane != car.endLane)
    {
        //switching...
        m_switchLane = 0;
    }
    
    if (std::fabs(m_simCar.speed - car.speed) > 0.0001) {
        if (car.speed != 0) {
            std::cout << std::endl << "speed diff  " << m_simCar.speed << " " << car.speed << std::endl;
            // this could be a bump -> restart the simulation...
            if (std::fabs(m_simCar.speed - car.speed) > (m_maxSpeed * m_turboMultip / m_accCoeff)) {
                m_cars.clear();
                m_bumpTick = gameTick;
                std::cout << "BUMP!! " << std::endl;
            }
            if (std::fabs(m_simCar.speed - car.speed) < 0.01) {
                m_cInfo.cars[m_color].speed = m_simCar.speed;
            }
            else {
//                 m_simCar = car;
            }
            //std::cout << "inPieceDist " << m_simCar.inPieceDist << " " << car.inPieceDist << " d: " << m_simCar.inPieceDist - car.inPieceDist<< std::endl;
            //std::cout << "prevDistSwitch    " << m_tInfo.length(car.piece-1, startLane, endLane) << std::endl;
            //std::cout << "prevDist    " << m_tInfo.length(car.piece-1, startLane, startLane) << std::endl;
            if (m_crash) {
                std::cout << "crash?" << std::endl;
//                 m_simCar = car;
                m_crash = false;
                m_throttle = 0;
                trackThrottle();
                return { make_throttle(m_throttle) };
            }
        }
        else {
            m_simCar = car;
            m_crash = true;
            m_throttle = 1.0;
            trackThrottle();
            return { make_throttle(m_throttle) };
        }
    }
    
    //std::cout << switchLane << " " << m_switchLane << std::endl;
    bool switchingOnCurve = false;
    bool warningSwitchSlowDown = false;
    
    
    
    //get next piece index with switch, we want to send the switch command as late as possible
    unsigned int firstSwitch = car.piece + 1;
    
    if (firstSwitch >= m_tInfo.pieceCount()) firstSwitch = 0;
    
    while (!m_tInfo.hasSwitch(firstSwitch) && firstSwitch != car.piece)
    {
        firstSwitch++;
        if (firstSwitch >= m_tInfo.pieceCount()) firstSwitch = 0;
    }
    
    bool canSwitch =  m_tInfo.distToPiece(car.piece, car.inPieceDist, car.startLane, car.endLane, firstSwitch) / 15 < car.speed;
    
    
    int followDir = followTarget();
    if(followDir != 0 && m_switchLane != followDir)
    {
        if (followDir < 0) {
            m_switchLane = -1;
            trackThrottle();
            // Apply last throttle and lane change before simulating next steps
            m_simCar = simulateStep(car, m_throttle, m_switchLane);
            return { make_request("switchLane", "Left")};
        }
        if (followDir > 0) {
            m_switchLane = 1;
            trackThrottle();
            // Apply last throttle and lane change before simulating next steps
            m_simCar = simulateStep(car, m_throttle, m_switchLane);
            return { make_request("switchLane", "Right")};
        }
    }
    
    //TODO: need to escape if there is someone behind, switch to less optimal lane
    
    double throttle = 0;
    double slideMaxSimAngle = 0;            
    double limit = m_maxAngleLimit;
    
//     std::cout << "Casual time! " << std::endl;
    double slideMaxSim = newMaximalThrottle(m_simCar, m_switchLane, switchingOnCurve);
    warningSwitchSlowDown |= switchingOnCurve;
    double slideMaxServ = newMaximalThrottle(car, m_switchLane, switchingOnCurve);
    warningSwitchSlowDown |= switchingOnCurve;
    
    slideMaxSimAngle = std::max(slideMaxSim, slideMaxServ);
    
    if(warningSwitchSlowDown)
    {
        //         std::cout << "Warning, switch ahead! "<< car.piece << std::endl;
        limit = m_maxAngleLimit - 1;
    }
    
    if(m_optimize)
    {
        limit = m_maxAngleLimit - 10;
    }
    
    throttle = (slideMaxSimAngle > limit) ? 0 : 1;
    
    
    
    bool evilWarning = false;
    //lets see if we can do some evil stuff :)
    if(canBeEvil(evilWarning))
    {
        m_trackThrottle = false; //dont track throttle for turbo if we plan to break using other car
        throttle = 1.0;
    }
    else if(evilWarning)
    {
        throttle = 0.0;
    }
    
    //TODO: overtaking should be prioritized over ramming -> it is somehow....
    

    
//     if(carAhead())
//     {
//         limit -= 4;
//     }
    
//     std::cout << m_simCar.speed << " " << m_simCar.angle << " " << m_simCar.angleSpeed << " "
//     << m_tInfo.radius(m_simCar.piece, m_simCar.inPieceDist, m_simCar.startLane, m_simCar.endLane) << " " 
//     << m_simCar.piece << " " << m_simCar.inPieceDist << " " 
//     << " lanes: " << m_simCar.startLane << " " << m_simCar.endLane
//     << "  sim "<< slideMaxSim  << " " << std::endl;
//     
//     std::cout << car.speed << " " << car.angle << " " << car.angleSpeed << " "
//     << m_tInfo.radius(car.piece, car.inPieceDist, car.startLane, car.endLane) << " " 
//     << car.piece << " " << car.inPieceDist << " " 
//     << " lanes: " << car.startLane << " " << car.endLane
//     << "  sim "<< slideMaxServ << " " << std::endl;
    
    
//     std::cout<< "  switch lane " << m_switchLane << " " << std::endl;
    
    
//     std::cout << m_simCar.speed << " " << m_simCar.angle << " " << m_simCar.angleSpeed << " "
//     << m_tInfo.radius(m_simCar.piece, m_simCar.inPieceDist, m_simCar.startLane, m_simCar.endLane)
//     << " lanes: " << startLane << " " << endLane << " : " << m_simCar.startLane << " " << m_simCar.endLane
//     << "  sim "<< slideMaxSim << "  " << slideMaxServ << " " << throttle << std::endl;
//     
//     std::cout << car.speed << " " << car.angle << " " << car.angleSpeed << " "
//     << m_tInfo.radius(car.piece, car.inPieceDist, car.startLane, car.endLane)
//     << " lanes: " << startLane << " " << endLane << " : " << car.startLane << " " << car.endLane
//     << "  sim "<< slideMaxSim << "  " << slideMaxServ << " " << throttle << std::endl;
    
    if(m_throttle != throttle)
    {
        
//         std::cout << "Throttle change to: " << throttle << std::endl;
        m_throttle = throttle;
        trackThrottle();
        // Apply last throttle and lane change before simulating next steps
        m_simCar = simulateStep(car, m_throttle, m_switchLane);
    }
    else{
        
        trackThrottle();
        // Apply last throttle and lane change before simulating next steps
        m_simCar = simulateStep(car, m_throttle, m_switchLane);
        if (m_turboAvailable > 0)
        {             
            if(useTurbo_v2() && slideMaxSimAngle < limit - 1)
            {
                std::cout << "Use turbo" << " " << slideMaxSimAngle << std::endl;
                return { make_turbo("Speed of light!!")};
            }
            
//             if (m_tInfo.useTurbo(car.piece)  && slideMaxSim < limit - 1) {
//                 std::cout << "Use turbo" << " " << slideMaxSim << std::endl;
//                 return { make_turbo("Speed of light!!")};
//             }
//             if (m_tInfo.useTurbo(car.piece+1) && slideMaxSim < limit - 5) {
//                 std::cout << "Use turbo before" << std::endl;
//                 return { make_turbo("Speed of light!!")};
//             }
//             
//             double turboLimit = turboThrottle(car, car.endLane-car.startLane,switchingOnCurve);
//             if(switchingOnCurve && limit > m_maxAngleLimit - 2) limit = m_maxAngleLimit - 2;
//             
//             if (turboLimit < limit - 3) {
//                 std::cout << "Use turboThrottle" << std::endl;
//                 return { make_turbo("Speed of light!!")};
//             }
        }
        
        // overtaking
        if (canSwitch && m_lockedTarget.size() == 0 && !m_optimize){
            // over taking
//             int safeLane = goSafeLane() + m_switchLane;
            bool overtaking = false;
            int overt = overtake(overtaking);
            

            
            if(overt == 0 && m_switchLane != 0 && overtaking)
            {
                if(car.endLane == m_tInfo.numLanes() - 1)
                {
                    overt = 1;
                }
                else
                {
                    overt = -1;
                }
            }
            
            //TODO: check if overtaking should have priority over going onto safe lane, enable safeLane
            if(overt == 0 && !overtaking)
            {
                overt = goSafeLane(overtaking);
            }
            
            if(overt == 0 && m_switchLane != 0 && overtaking)
            {
                if(car.endLane == m_tInfo.numLanes() - 1)
                {
                    overt = 1;
                }
                else
                {
                    overt = -1;
                }
            }

            
            if(overt != 0)
            {
                double overtakeLimit = newMaximalThrottle(car, overt, switchingOnCurve);
                if(switchingOnCurve && limit > m_maxAngleLimit - 1) limit = m_maxAngleLimit - 1;
                
                if (overt < 0 && overtakeLimit < limit) {
                    m_switchLane = -1;
                    std::cout << "Overtake Left " << car.piece << " " << m_tInfo.radius(car.piece, car.inPieceDist, car.startLane, car.endLane) << std::endl;
                    return { make_request("switchLane", "Left")};
                }
                if (overt > 0 && overtakeLimit < limit) {
                    m_switchLane = 1;
                    std::cout << "OverTake Right " << car.piece << " " << m_tInfo.radius(car.piece, car.inPieceDist, car.startLane, car.endLane) << std::endl;
                    return { make_request("switchLane", "Right")};
                }
            }
            
            //TODO: make sure that I switch where I need to switch... like overtaking
            int switchLaneDir = m_tInfo.switchLane(car.piece, car.endLane);
            if(switchLaneDir != 0)
            {
                double switchLimit = newMaximalThrottle(car, switchLaneDir, switchingOnCurve);
                if(switchingOnCurve && limit > m_maxAngleLimit - 1) limit = m_maxAngleLimit - 1;
                // switching to optimal lane
                if (switchLaneDir < 0 && switchLimit < limit) {
                    m_switchLane = -1;
                    //std::cout << "Switch lane Left " << car.piece << " " << m_tInfo.radius(car.piece, car.startLane, car.endLane) << std::endl;
                    return { make_request("switchLane", "Left")};
                }
                if (switchLaneDir > 0 && switchLimit < limit) {
                    m_switchLane = 1;
                    //std::cout << "Switch lane Right " << car.piece << " " << m_tInfo.radius(car.piece, car.startLane, car.endLane) << std::endl;
                    return { make_request("switchLane", "Right")};
                }
            }
        }
    }
    
    return { make_throttle(m_throttle) };
}

void game_logic::findBestTurboPosition(const CarsInfo::Car &ccar)
{
    boost::timer t;
    CarsInfo::Car car = ccar;
    std::vector<double> throttleList = {1.0,1.0,1.0,1.0,0.0};
    
    int switchLane = 0;
    double crashDist = 0.0;
    bool switchOnCurve = false;
    double maxAngle = 0;
    
    int throttleCount = 0;
    double throttleDist = 0.0;
    double throttle = 0.0;
    
    double simStopDist = (ccar.lap + 1) * 1000 + ccar.piece + 1;
    bool readyToLogData = false;
    
    
    std::cout << car.lap*1000 + car.piece << " < " << simStopDist << std::endl;
    
    while(car.lap*1000 + car.piece <= simStopDist)
    {
        switchLane = m_tInfo.switchLane(car.piece, car.endLane);
        maxAngle = simulateMaxAngle(car, switchLane, switchOnCurve, crashDist, throttleList);
        if(maxAngle >= 60)
        {
            throttle = 0.0;
            if(!readyToLogData)
            {
                readyToLogData = true;
                simStopDist = (car.lap + 1) * 1000 + car.piece + 1;
            }
            else
            {
                if(throttleCount > 2)
                {
                    
                    //                 std::cout << "Enter pair: " << throttleDist << "," << throttleCount << std::endl;
                    m_throttle_dist_map.insert(std::pair<int,double>(throttleCount, throttleDist));
                    m_dist_throttle_map.insert(std::pair<double,int>(throttleDist, throttleCount));
                }
            }
            throttleCount = 0;
        }
        else
        {
            throttle = 1.0;
            if(throttleCount == 0)
            {
                //assuming that the piece length will never be longer that 1000 :)
                throttleDist = car.piece*1000 + car.inPieceDist;
            }
            throttleCount++;
        }
         
        car =  game_logic::simulateStep(car, throttle, switchLane, 1);
//         std::cout 
//         << "speed: " << car.speed
//         << "angle: " << car.angle
//         << "max: " << maxAngle
//         << "throttle: " << throttle
//         << std::endl;
        if(readyToLogData)
        {
            m_defaultSpeed.insert(std::pair<double, double>(car.piece*1000 + car.inPieceDist, car.speed));
        }
//         std::cout << car.piece << ", " << car.inPieceDist << ", " << car.speed << ", " << car.angle << "," << maxAngle << ", " << throttle << std::endl;
    }
    
    if(m_throttle_dist_map.size() == 0)
    {
        unsigned int turboPiece = 0;
        while(!m_tInfo.useTurbo(turboPiece))
        {
            turboPiece++;
            if(turboPiece >= m_tInfo.pieceCount())
            {
                turboPiece = 0;
                break;
            }
        }
        
        throttleDist = turboPiece * 1000;
        throttleCount = 10;
    
        m_throttle_dist_map.insert(std::pair<int,double>(throttleCount, throttleDist));
        m_dist_throttle_map.insert(std::pair<double,int>(throttleDist, throttleCount));
        
        if(m_defaultSpeed.size() == 0)
        {
            m_defaultSpeed.insert(std::pair<double, double>(car.piece*1000 + car.inPieceDist, car.speed));
        }
        
        std::map<int,double>::reverse_iterator it_acc = m_throttle_dist_map.rbegin();
        std::cout << "Best place: " << it_acc->second << " " << it_acc->first << std::endl;
        std::cout << "Simulated ticks: " << m_defaultSpeed.size() << std::endl;
    }
    else
    {
        std::map<int,double>::reverse_iterator it_acc = m_throttle_dist_map.rbegin();
        std::cout << "Best place: " << it_acc->second << " " << it_acc->first << std::endl;
        it_acc++;
        std::cout << "Second place: " << it_acc->second << " " << it_acc->first << std::endl;
        it_acc++;
        std::cout << "Third place: " << it_acc->second << " " << it_acc->first << std::endl;
    }
    
    std::cout << "Finding best turbo pos took: " << t.elapsed() << std::endl;

}

bool game_logic::useTurbo_v2()
{
    bool retVal = false;
    
    if(m_dist_throttle_map.size() != 0 && 
        m_throttle_dist_map.size() != 0 && 
        m_turboAvailable > 0 )
    {
        
        double curr_dist = m_cInfo.cars[m_color].piece*1000 + m_cInfo.cars[m_color].inPieceDist;

        
        std::map<int,double>::reverse_iterator it_acc = m_throttle_dist_map.rbegin();
        
        double bestCount = it_acc->first;
        
        while(it_acc->first >= bestCount * 0.99)
        {
            double distDiff = curr_dist - it_acc->second;
            
            //              std::cout << "-Dist....: " << distDiff << std::endl;
            if(distDiff < -2000)
            {
                distDiff = distDiff + m_tInfo.pieceCount() * 1000;
            }
            
            if(distDiff < 2000)
            {
                //simulate if we dont crash if we turbo now
                std::vector<double> throttleList;
                int turboCount = bestCount / 10;
                for(int i = 0; i < turboCount; ++i)
                {
                    throttleList.push_back(m_turboMultipAvailable);
                }
                throttleList.push_back(0.0);
                double crashDist = 0.0;
                bool switchingOnCurve = false;
                if(simulateMaxAngle(m_cInfo.cars[m_color], m_switchLane, switchingOnCurve, crashDist, throttleList) < 59)
                {
                    retVal = true;
                }
            }
            else
            {
                //                  std::cout << "-Not... to far from optimal location--" << curr_dist << " " << distDiff << std::endl;
            }
            it_acc++;
            if(it_acc == m_throttle_dist_map.rend()) break;
        }

    }
    
    return retVal;
}

void game_logic::trackThrottle()
{
//     if(m_gameTick - m_bumpTick < 20)
//     {
//         m_trackThrottle = false;
//     }
//     
//     if(m_throttle < 0.01 && !m_trackThrottle)
//     {
//         m_trackThrottle = true;
//     }
//     
//     if( m_trackThrottle &&
//         m_throttle > 0.99 && 
//         m_cInfo.cars[m_color].speed != 0 &&
//         (std::fabs(m_cInfo.cars[m_color].angle) < 50 || m_tInfo.radius(m_cInfo.cars[m_color].piece, m_cInfo.cars[m_color].inPieceDist, m_cInfo.cars[m_color].startLane, m_cInfo.cars[m_color].endLane) > 1000)
//     )
//     {
//         if(m_throttleCount == 0)
//         {
//             //assuming that the piece length will never be longer that 1000 :)
//             m_throttleDist = m_cInfo.cars[m_color].piece*1000 + m_cInfo.cars[m_color].inPieceDist;
//         }
//         m_throttleCount++;
//     }
//     else
//     {
//         if(m_throttleCount > 2)
//         {
//             m_throttle_dist_map.insert(std::pair<int,double>(m_throttleCount, m_throttleDist));
//             m_dist_throttle_map.insert(std::pair<double,int>(m_throttleDist, m_throttleCount));
//         }
//         m_throttleCount = 0;
//     }
}

double game_logic::simulateMaxAngle(const CarsInfo::Car &ccar, int switchLane, bool &switchOnCurve, double &crashDist, const std::vector<double> & throttleList)
{
    switchOnCurve = false;
    double maxSlide = 0;
    double maxASpeed = 0;
    double maxAAcc = 0;
    double dist = 0;
    crashDist = 0;
    
    //1st go with 1,1,1,1,0,0,0,0,0,0,0,0,0... as throttle
    //2nd go with 1,0,0,0,0,0,0,0,0,0,0,0,0... as throttle
    CarsInfo::Car car = ccar;
    
    double radius = m_tInfo.radius(car.piece, car.inPieceDist, car.startLane, car.endLane);
    if(radius < 1000 && car.startLane != car.endLane)
    {
        switchOnCurve = true;
    }
    
    unsigned int origEndLane = car.endLane;
    
    unsigned int i = 0;
    double throttle = 0;
    
    // Simulate what happens if we now set throttle 1.0 and then 0.0 for x ticks
    
    while (car.speed > 2) { // FIXME find the tightest curve and calculate the slip speed -> limit
        
        if(i < throttleList.size() - 1)
        {
            throttle = throttleList[i];
        }
        else if(throttleList.size() > 0)
        {
            throttle = throttleList[throttleList.size() - 1];
        }
        else
        {
            throttle = 0;
        }
        i++;
        
        if(std::fabs(car.angle) >= 60 && crashDist < 1)
        {
            maxSlide = std::max(std::fabs(car.angle), maxSlide);
            crashDist = dist;
            return maxSlide;
        }
        
        
        car = simulateStep(car, throttle, switchLane, 0);
//         std::cout 
//         << "speed: " << car.speed
//         << "angle: " << car.angle
//         << "throttle: " << throttle
//         << std::endl;
        
        dist += car.speed; 
        radius = m_tInfo.radius(car.piece, car.inPieceDist, car.startLane, car.endLane);
        if(radius < 1000 && car.startLane != car.endLane)
        {
            switchOnCurve = true;
        }
        if (origEndLane != car.endLane && switchLane != 0) {
            
            switchLane = 0;
        }
        
        maxSlide = std::max(std::fabs(car.angle), maxSlide);
        maxASpeed = std::max(std::fabs(car.angleSpeed), maxASpeed);
        maxAAcc = std::max(std::fabs(car.angleAcc), maxAAcc);
    }
    return maxSlide;
}

double game_logic::newMaximalThrottle(const CarsInfo::Car &ccar, int switchLane, bool &switchOnCurve, double customTurbo, double turboDuration)
{
    switchOnCurve = false;
    double maxSlide = 0;
    double maxASpeed = 0;
    double maxAAcc = 0;
    
    int maxCount = 4;  // first add speed for max speed
    
    //1st go with 1,1,1,1,0,0,0,0,0,0,0,0,0... as throttle
    //2nd go with 1,0,0,0,0,0,0,0,0,0,0,0,0... as throttle
    for(int i= 0; i < 2; i++)
    {
        CarsInfo::Car car = ccar;
        
        double radius = m_tInfo.radius(car.piece, car.inPieceDist, car.startLane, car.endLane);
        if(radius < 1000 && car.startLane != car.endLane)
        {
            switchOnCurve = true;
        }
        
        unsigned int origEndLane = car.endLane;
        
        // Simulate what happens if we now set throttle 1.0 and then 0.0 for x ticks
        
        while (car.speed > 2) { // FIXME find the tightest curve and calculate the slip speed -> limit
            
            
            car = simulateStep(car, (maxCount > 0 ? 1.0 : 0), switchLane);
            radius = m_tInfo.radius(car.piece, car.inPieceDist, car.startLane, car.endLane);
            if(radius < 1000 && car.startLane != car.endLane)
            {
                switchOnCurve = true;
            }
            if (origEndLane != car.endLane && switchLane != 0) {
                
                switchLane = 0;
            }
            
            maxSlide = std::max(std::fabs(car.angle), maxSlide);
            maxASpeed = std::max(std::fabs(car.angleSpeed), maxASpeed);
            maxAAcc = std::max(std::fabs(car.angleAcc), maxAAcc);
            maxCount--;
        }
        maxCount = 1;
    }
    //std::cout << maxSlide << " " << maxASpeed << " aac: " << maxAAcc << "   ";
    
    // no crash -> full speed ahead :)
    return maxSlide;
}

double game_logic::turboThrottle(const CarsInfo::Car &ccar, int switchLane, bool &switchOnCurve)
{
    CarsInfo::Car car = ccar;
    switchOnCurve = false;
    int turboCount = m_turboAvailable;
    double maxSlide = 0;
    double maxASpeed = 0;
    double maxAAcc = 0;
    
    double radius = 0;
    bool hasSwitch = false;
    
    // Simulate what happens if we now use turbo and acelerate for the whole turbo
    while (turboCount > 0) {
        radius = m_tInfo.radius(car.piece, car.inPieceDist, car.startLane, car.endLane);
        hasSwitch = m_tInfo.hasSwitch(car.piece);
        car = simulateStep(car, m_turboMultipAvailable, switchLane);
        if (car.startLane + switchLane == car.endLane && switchLane != 0) {
            if(radius < 1000 && hasSwitch)
            {
                switchOnCurve = true;
            }
            switchLane = 0;
        }
        
        maxSlide = std::max(std::fabs(car.angle+car.angleSpeed), maxSlide);
        maxASpeed = std::max(std::fabs(car.angleSpeed), maxASpeed);
        maxAAcc = std::max(std::fabs(car.angleAcc), maxAAcc);
        turboCount--;
    }
    // then break
    while (car.speed > 2) {
        radius = m_tInfo.radius(car.piece, car.inPieceDist, car.startLane, car.endLane);
        hasSwitch = m_tInfo.hasSwitch(car.piece);
        car = simulateStep(car, 0, switchLane);
        if (car.startLane + switchLane == car.endLane && switchLane != 0) {
            if(radius < 1000 && hasSwitch)
            {
                switchOnCurve = true;
            }
            switchLane = 0;
        }
        maxSlide = std::max(std::fabs(car.angle), maxSlide);
        maxASpeed = std::max(std::fabs(car.angleSpeed), maxASpeed);
        maxAAcc = std::max(std::fabs(car.angleAcc), maxAAcc);
        turboCount--;
    }
    return maxSlide;
}


CarsInfo::Car game_logic::simulateStep(const CarsInfo::Car &ccar, double throttle, int switchLane, double customTurbo)
{
    CarsInfo::Car car = ccar;
    
    double turboMultip = m_turboMultip;
    
    if(customTurbo > 0.01)
    {
        turboMultip = customTurbo;
    }
    
    // calculate angle according to last speed
    double radius = m_tInfo.radius(car.piece, car.inPieceDist, car.startLane, car.endLane);
    double direction = radius / std::fabs(radius);
    double normalAcc = direction * car.speed / sqrt(std::fabs(radius)) * m_multiplier;
    
    double torque = m_torque;
    
    if(m_optimize)
        torque -= 0.05;
    
    if (normalAcc > torque) {
        normalAcc -= torque;
    }
    else if (normalAcc < - torque) {
        normalAcc += torque;
    }
    else {
        normalAcc = 0;
    }
    
    if(radius > 1000)
    {
        normalAcc = 0;
    }
    
    double springAcc = car.angle * m_springConst;
    
    car.angleAcc = (normalAcc - springAcc) * car.speed;
    
    car.angleSpeed *= m_damping;
    car.angleSpeed += car.angleAcc;
    
    car.angle += car.angleSpeed;
    
    // calculate new speed and position
    car.acc = (throttle*m_maxSpeed*turboMultip - car.speed)/m_accCoeff;
    car.speed += car.acc;
    
    car.inPieceDist += car.speed;
    if (car.inPieceDist > m_tInfo.length(car.piece, car.startLane, car.endLane)) {
        car.inPieceDist -= m_tInfo.length(car.piece, car.startLane, car.endLane);
        
        car.piece++;
        if (m_tInfo.hasSwitch(car.piece)) {
            car.endLane = std::max(0, std::min((int)(car.startLane + switchLane), (int)m_tInfo.numLanes()-1));
        }
        else
        {
            car.startLane = car.endLane;
        }
        if (car.piece >= m_tInfo.pieceCount()) {
            car.lap++;
            car.piece = 0;
        }
    }
    
    return car;
}

// void game_logic::adjustSpringConst()
// {
//     double angle1 = 0.0;
//     double angle2 = 0.0;
//     double angle3 = 0.0;
//     
//     double angleAcc1 = angle2 - angle1;
//     double angleAccDiff = angleAcc1 * m_damping - (angle3 - angle2);
//     double springConst = angleAccDiff / speed2 / angle2;
//     
// }

void game_logic::adjustParameters_v2()
{
    if(m_cars.size() > 3)
    {
        double oM = 0;
        double oT = 0;
        double oS = 0;
        //lets make assumption that all radiuses should be equal
        for(unsigned int i = 0; i < m_cars.size() - 4; ++i)
        {
            double radius1 = m_tInfo.radius(m_cars[i].piece, m_cars[i].inPieceDist, m_cars[i].startLane, m_cars[i].endLane);
            double radius2 = m_tInfo.radius(m_cars[i+1].piece, m_cars[i+1].inPieceDist, m_cars[i+1].startLane, m_cars[i+1].endLane);
            double radius3 = m_tInfo.radius(m_cars[i+2].piece, m_cars[i+2].inPieceDist, m_cars[i+2].startLane, m_cars[i+2].endLane);
            double radius4 = m_tInfo.radius(m_cars[i+3].piece, m_cars[i+3].inPieceDist, m_cars[i+3].startLane, m_cars[i+3].endLane);
            if(radius1 == radius2 && radius1 == radius3 && radius1 == radius4 && radius1 < 1000)
            {
                //also this method assumes that the angle is != 0 and angle and radius have the same sign
                if(m_cars[i].angle != 0 &&
                    m_cars[i+1].angle != 0 &&
                    m_cars[i+2].angle != 0 &&
                    m_cars[i+3].angle != 0 &&
                    radius1 / m_cars[i+0].angle > 0 &&
                    radius1 / m_cars[i+1].angle > 0 &&
                    radius1 / m_cars[i+2].angle > 0 &&
                    radius1 / m_cars[i+3].angle > 0 &&
                    !m_tInfo.hasSwitch(m_cars[i].piece) &&
                    !m_tInfo.hasSwitch(m_cars[i+1].piece) &&
                    !m_tInfo.hasSwitch(m_cars[i+2].piece) &&
                    !m_tInfo.hasSwitch(m_cars[i+3].piece)
                )
                {
                    double SQRTR = sqrt(std::fabs(radius1));
                    double A1 = std::fabs(m_cars[i+0].angle);
                    double A2 = std::fabs(m_cars[i+1].angle);
                    double A3 = std::fabs(m_cars[i+2].angle);
                    double V1 = m_cars[i+0].speed;
                    double V2 = m_cars[i+1].speed;
                    double V3 = m_cars[i+2].speed;
                    double X1 = std::fabs(m_cars[i+1].angleSpeed - m_cars[i+0].angleSpeed * 0.9);
                    double X2 = std::fabs(m_cars[i+2].angleSpeed - m_cars[i+1].angleSpeed * 0.9);
                    double X3 = std::fabs(m_cars[i+3].angleSpeed - m_cars[i+2].angleSpeed * 0.9);
                    
                    double Q1 = X1 / A1 / V1;
                    double Q2 = V1 / A1 / SQRTR;
                    double Q3 = V2 / SQRTR - A2 * Q2;
                    double Q4 = X2 / V2 - A2 * Q1;
                    double Q5 = 1 - A2  /A1;
                    double Q6 = Q3 / Q5;
                    double Q7 = Q4 / Q5;
                    double Q8 = V3 / SQRTR - Q6 - A3 * (Q2 - Q6 / A1 );
                    double Q9 = X3 / V3 - Q7 - A3 * Q1 + A3 * Q7 / A1;
                    
                    double M = Q9 / Q8;
                    double T = M * Q6 - Q7;
                    double S = M * Q2 - Q1 - T / A1;
                    
                    //                     std::cout << "Piece: " << m_cars[i].piece
                    //                     << ",InPieceDist: " << m_cars[i].inPieceDist
                    //                     << ",Radius: " << std::fabs(radius1)
                    //                     << ",Angle: " << m_cars[i].angle
                    //                     <<std::endl;
                    //                     
                    //                     std::cout << "Piece: " << m_cars[i+1].piece
                    //                     << ",InPieceDist: " << m_cars[i+1].inPieceDist
                    //                     << ",Radius: " << std::fabs(radius1)
                    //                     << ",Angle: " << m_cars[i+1].angle
                    //                     <<std::endl;
                    //                     
                    //                     std::cout << "Piece: " << m_cars[i+2].piece
                    //                     << ",InPieceDist: " << m_cars[i+2].inPieceDist
                    //                     << ",Radius: " << std::fabs(radius1)
                    //                     << ",Angle: " << m_cars[i+2].angle
                    //                     <<std::endl;
                    //                     
                    //                     std::cout << "Piece: " << m_cars[i+3].piece
                    //                     << ",InPieceDist: " << m_cars[i+3].inPieceDist
                    //                     << ",Radius: " << std::fabs(radius1)
                    //                     << ",Angle: " << m_cars[i+3].angle
                    //                     <<std::endl;
                    
                    //Someting weird happens sometimes :) Just make sure we detected same vers twice in a row
                    if(std::fabs(oM-M) < 0.0000001 &&
                        std::fabs(oT-T) < 0.0000001 &&
                        std::fabs(oS-S) < 0.0000001 &&
                        std::fabs(T) > 0.01 &&
                        std::fabs(M) > 0.01 &&
                        std::fabs(S) > 0.0001
                    )
                    {
                        m_torque = std::fabs(T);
                        m_springConst = std::fabs(S);
                        m_multiplier = std::fabs(M);
                        m_maxAngleLimit = 60;
                        
                        std::cout << "M: " << m_multiplier
                        << ", T: " << m_torque
                        << ", S: " << m_springConst
                        <<std::endl;
                        m_optimize = false;
                        
                        
                        findBestTurboPosition(m_cInfo.cars[m_color]);
                        return;
                    }
                    oM = M;
                    oT = T;
                    oS = S;
                }
                m_cars.erase (m_cars.begin(),m_cars.begin()+i);
            }
        }
    }
}

void game_logic::adjustParameters()
{
    adjustParameters_v2();
    return;
    boost::timer t;
    
    m_multiplier = 0.53;
    m_springConst = 0.00125;
    //     m_torque = 0.32;
    
    double vAMax=0;
    double c_vAMax = 0;
    double aMax = 0;
    double c_aMax = 0;
    double accDiff = 100000000;
    //     double accDiffOld = 100000000;
    double angleDiff = 100000000;
    
    // Get the initial multiplier
    for (size_t i=0; i<m_cars.size(); i++) {
        vAMax = std::max(vAMax, std::fabs(m_cars[i].angleSpeed));
        aMax = std::max(std::fabs(m_cars[i].angle), aMax);
    }
    
    //     double accDiffOld = 100000000;
    double angleDiffOld = 100000000;
    simulateParameters(c_vAMax, c_aMax, accDiff, angleDiff);
    angleDiffOld = angleDiff;
    
    double adjustmentMulti = angleDiffOld/1000;
    
    for(int i = 0; i < 1000; ++i)
    {
        adjustmentMulti = angleDiffOld/1000;
        
        m_torque += 0.0001 * adjustmentMulti;
        simulateParameters(c_vAMax, c_aMax, accDiff, angleDiff);
        if(angleDiffOld < angleDiff)
        {
            m_torque -= 0.0001 * adjustmentMulti;
        }
        else
        {
            while(angleDiffOld > angleDiff)
            {
                angleDiffOld = angleDiff;
                m_torque += 0.0001 * adjustmentMulti;
                simulateParameters(c_vAMax, c_aMax, accDiff, angleDiff);
            }
            m_torque -= 0.0001 * adjustmentMulti;
        }
        m_torque -= 0.0001 * adjustmentMulti;
        simulateParameters(c_vAMax, c_aMax, accDiff, angleDiff);
        
        
        while (angleDiffOld > angleDiff) {
            //         if (m_torque < m_torqueMin - 0.005) break;
            angleDiffOld = angleDiff;
            m_torque -= 0.0001 * adjustmentMulti;
            simulateParameters(c_vAMax, c_aMax, accDiff, angleDiff);
            std::cout << "0 params: " << m_multiplier << " " << m_torque << " " << m_springConst << " " << accDiff << " " << angleDiff << std::endl;
        }
        
        m_torque += 0.0001 * adjustmentMulti;
        
        
        m_springConst += 0.000001 * adjustmentMulti;
        simulateParameters(c_vAMax, c_aMax, accDiff, angleDiff);
        if(angleDiffOld < angleDiff)
        {
            m_springConst -= 0.000001 * adjustmentMulti;
        }
        else
        {
            while(angleDiffOld > angleDiff)
            {
                angleDiffOld = angleDiff;
                m_springConst += 0.000001 * adjustmentMulti;
                simulateParameters(c_vAMax, c_aMax, accDiff, angleDiff);
            }
            m_springConst -= 0.000001 * adjustmentMulti;
        }
        m_springConst -= 0.000001 * adjustmentMulti;
        simulateParameters(c_vAMax, c_aMax, accDiff, angleDiff);
        
        // spring constant
        while (angleDiffOld > angleDiff) {
            angleDiffOld = angleDiff;
            //m_multiplier = m_multiplier * vAMax / c_vAMax;
            m_springConst -=0.000001 * adjustmentMulti;
            simulateParameters(c_vAMax, c_aMax, accDiff, angleDiff);
            std::cout << "1 params: " << m_multiplier << " " << m_torque << " " << m_springConst << " " << accDiff << " " << angleDiff << std::endl;
        }
        m_springConst += 0.000001 * adjustmentMulti;
        //     angleDiffOld = 100000000;
        m_multiplier += 0.0001 * adjustmentMulti;
        simulateParameters(c_vAMax, c_aMax, accDiff, angleDiff);
        if(angleDiffOld < angleDiff)
        {
            m_multiplier -= 0.0001 * adjustmentMulti;
        }
        else
        {
            while(angleDiffOld > angleDiff)
            {
                angleDiffOld = angleDiff;
                m_multiplier += 0.0001 * adjustmentMulti;
                simulateParameters(c_vAMax, c_aMax, accDiff, angleDiff);
            }
            m_multiplier -= 0.0001 * adjustmentMulti;
        }
        m_multiplier -= 0.0001 * adjustmentMulti;
        simulateParameters(c_vAMax, c_aMax, accDiff, angleDiff);
        
        while (angleDiffOld > angleDiff) {
            angleDiffOld = angleDiff;
            m_multiplier -= 0.0001 * adjustmentMulti;
            simulateParameters(c_vAMax, c_aMax, accDiff, angleDiff);
            std::cout << "2 params: " << m_multiplier << " " << m_torque << " " << m_springConst << " " << accDiff << " " << angleDiff << std::endl;
        }
        
        m_multiplier += 0.0001 * adjustmentMulti;
    }
    
    simulateParameters(c_vAMax, c_aMax, accDiff, angleDiff);
    //     m_maxAngleLimit = 60 - std::fabs(accDiff) / 300;
    std::cout << "Final params: " << m_multiplier << " " << m_torque << " " << m_springConst << " " << accDiff << " " << angleDiff << std::endl;
    std::cout << "Final angle: " << m_maxAngleLimit << std::endl;
    
    m_maxAngleLimit = 60;
    //     m_torque = 0.3;
    //     m_springConst = 0.00125;
    //     m_multiplier = 0.53;
    
    std::cout << "Adjusting parameters took: " << t.elapsed() << std::endl;
}

void game_logic::simulateParameters(double &c_vAMax, double &c_aMax, double &accDiff, double &angleDiff)
{
    double angle = 0;
    double angleSpeed = 0;
    double angleAcc = 0;
    
    c_vAMax = 0;
    c_aMax = 0;
    accDiff = 0;
    angleDiff = 0;
    
    // simulate the slide with multiplier==1 then adjust the multiplier get the same result
    for (size_t i=0; i<m_cars.size()-1; i++) {
        // normal acceleration mass == 1
        double radius = m_tInfo.radius(m_cars[i].piece, m_cars[i].inPieceDist, m_cars[i].startLane, m_cars[i].endLane);
        double direction = radius / std::fabs(radius);
        double normalAcc = direction * m_cars[i].speed / sqrt(std::fabs(radius)) * m_multiplier;
        if (normalAcc > m_torque) {
            normalAcc -= m_torque;
        }
        else if (normalAcc < - m_torque) {
            normalAcc += m_torque;
        }
        else {
            normalAcc = 0;
        }
        
        if(radius > 1000)
        {
            normalAcc = 0;
        }
        
        double springAcc = angle * m_springConst;
        
        angleAcc = (normalAcc - springAcc) * m_cars[i].speed;
        
        angleSpeed *= m_damping;
        angleSpeed += angleAcc;
        
        angle += angleSpeed;
        
        c_vAMax = std::max(std::fabs(angleSpeed), c_vAMax);
        c_aMax = std::max(std::fabs(angle), c_aMax);
        accDiff += std::fabs(angleSpeed - m_cars[i+1].angleSpeed)*10;
        angleDiff += std::fabs(angle - m_cars[i+1].angle)*10;
        
        //std::cout << car[i].speed << " " << car[i].angle << " " << angle << std::endl;
    }
}

int game_logic::followTarget()
{
    if(m_lockedTarget.size() != 0)
    {
        CarsInfo::Car opcar = m_cInfo.cars[m_lockedTarget];
        if(opcar.speed < 1)
        {
            m_lockedTarget = "";
            return 0;
        }
        
        CarsInfo::Car owncar = m_cInfo.cars[m_color];
        
        if(opcar.endLane > owncar.endLane || opcar.endLane > owncar.endLane + m_switchLane)
            return 1;
        
        if(opcar.endLane < owncar.endLane || opcar.endLane < owncar.endLane + m_switchLane)
            return -1;
        
        std::vector<double> throttleList = {1.0, 0.0};
        bool switchingOnCurve = false;
        double crashDist = 0.0;
        if(m_cInfo.distToCar(owncar, opcar) < 41 || simulateMaxAngle(owncar, m_switchLane, switchingOnCurve, crashDist, throttleList) < 59)
        {
            m_lockedTarget = "";
            return 0;
        }
        
        //TODO: watch https://helloworldopen.com/race/53824f6de4b01affc2064319 -> r900 and forward, the car tried to ram yellow, then it noticed that yellow is too slow, so it tried to overtake it :)
    }
    else
    {
        return 0;
    }
    
    return 0;
}

bool game_logic::canBeEvil(bool &warning)
{
    warning = false;
 
    if(m_optimize)
    {
        return false;
    }
    
//     std::cout << __LINE__ << std::endl;
    //TODO: modify this if we or the other cas has turbo
    std::vector<double> optimalThrottleList = {1.0, 0.0};
    std::vector<double> ownThrottleList = {1.0};
    
    
    CarsInfo::Car opcar;
    CarsInfo::Car owncar = m_cInfo.cars[m_color];
    CarsInfo::Car optimalOwncar = m_cInfo.cars[m_color];
    
    int currentSwitchLane = m_switchLane;
    
    
    if(owncar.speed < 1)
        return false;
   
    std::map<std::string, CarsInfo::Car>::iterator it;

    //First get the closest car
    double dist = 300;
    std::string opcolor;
    for (it=m_cInfo.cars.begin(); it!=m_cInfo.cars.end(); it++) 
    {
//     std::cout << __LINE__ << std::endl;
        if (m_color.compare(it->first) == 0) continue;
        
        CarsInfo::Car opcar = it->second;
        //TODO: Can be improved maybe to check only endLane, but first need to ensure the car will have time to switch
        if (opcar.endLane != owncar.endLane || opcar.startLane != owncar.startLane) continue;
        
        double tempdist = m_cInfo.distToCar(owncar, opcar);
        
        if(tempdist > 0.01 && tempdist < dist)
        {
            dist = tempdist;
            opcolor = it->first;
        }
    }
    
    if(dist > 299)
    { 
        return false;
    }

    opcar = m_cInfo.cars[opcolor];
    
    //dont be evil to slower cars :)
    std::map<double, double>::iterator optimalSpeedIt = m_defaultSpeed.lower_bound(opcar.piece*1000 + opcar.inPieceDist);
    if(optimalSpeedIt == m_defaultSpeed.end())
    {
        optimalSpeedIt--;
    }
    
    bool opSoSlow = opcar.speed < optimalSpeedIt->second * 0.85;
    
//     std::cout 
//         << "target color: " << opcolor
//         << "dist: " << dist
//         << std::endl;
    
    //now simlate both cars, to see if we can catch the other car, before we crash :)
    
    
    //     std::cout << __LINE__ << std::endl;
    bool switchOnCurve = false;

    double opMaxAngle = std::fabs(opcar.angle);
    
    dist = m_cInfo.distToCar(owncar, opcar);
    
    while(dist < 300 && dist > 0 && opcar.speed > 1)
    {  
        //     std::cout << __LINE__ << std::endl;
        //we need to simulate the less optimal / faster lane
        int switchLane1 = m_tInfo.switchLane(opcar.piece, opcar.endLane);
        int switchLane2 = m_tInfo.reverseSwitchLane(opcar.piece, opcar.endLane);
        double crashDist1 = 0.0;
        double crashDist2 = 0.0;
        double angle1 = simulateMaxAngle(opcar, switchLane1, switchOnCurve, crashDist1, optimalThrottleList);
        double angle2 = simulateMaxAngle(opcar, switchLane2, switchOnCurve, crashDist2, optimalThrottleList);
        
        opMaxAngle = std::min(angle1, angle2);
        double opSwitchLane = angle1 < angle2 ? switchLane1 : switchLane2;
        double crashDist = angle1 < angle2 ? crashDist1 : crashDist2;
        
        if(opMaxAngle > 60)
        {
            //     std::cout << __LINE__ << std::endl;
            opcar =  game_logic::simulateStep(opcar, 0, opSwitchLane, 1);
        }
        else
        {
            //     std::cout << __LINE__ << std::endl;
            opcar =  game_logic::simulateStep(opcar, 1, opSwitchLane, 1);
        }
        
        int ownSwitchLane = m_tInfo.switchLane(owncar.piece, owncar.endLane);
        crashDist = 0.0;
        //simulate to get crashDist
        simulateMaxAngle(owncar, ownSwitchLane, switchOnCurve, crashDist, ownThrottleList);
//         std::cout 
//                 << ", crashDist: " << crashDist
//                 << ", ownMaxAngle: " << ownMaxAngle
//                 << std::endl;
        owncar =  game_logic::simulateStep(owncar, 1, ownSwitchLane, 1);
        
        dist = m_cInfo.distToCar(owncar, opcar);
        
        {
            //check that if we just ride notrmally we dont hit the car in front with angle larger than 30
            double crashDistTemp = 0.0;
            double optimalOwnMaxAngle = simulateMaxAngle(optimalOwncar, currentSwitchLane, switchOnCurve, crashDistTemp, optimalThrottleList);
            
            CarsInfo::Car carBeforeSim = optimalOwncar;
            if(optimalOwnMaxAngle > 60)
            {
                //     std::cout << __LINE__ << std::endl;
                optimalOwncar =  game_logic::simulateStep(optimalOwncar, 0, currentSwitchLane, 1);
            }
            else
            {
                //     std::cout << __LINE__ << std::endl;
                optimalOwncar =  game_logic::simulateStep(optimalOwncar, 1, currentSwitchLane, 1);
            }
        
            double normaldist = m_cInfo.distToCar(optimalOwncar, opcar);
            
            // Calculate max throttle
            if(carBeforeSim.startLane == carBeforeSim.endLane && optimalOwncar.startLane != optimalOwncar.endLane)
            {
                //switching...
                currentSwitchLane = 0;
            }
            
            if(normaldist < 60 && !(std::fabs(optimalOwncar.angle) < 30 && std::fabs(optimalOwncar.angleSpeed) < 2))
            {
//                 std::cout 
//                 << "Crash bump warning: "
//                 << std::endl;
                warning = true;
                return false;
            }
        }
        
        
        
        if(crashDist < dist /*-40*/ && crashDist > 0.001)
        {
            //     std::cout << __LINE__ << std::endl;
//             std::cout 
//                 << "Will crash... ignore the pursuit: "
//                 << "dist: " << dist
//                 << ", bump at angle: " << owncar.angle
//                 << ", crashDist: " << crashDist
//                 << std::endl;
            break;
        }
        else if(dist < 40)
        {
            //TODO: should change to 50? 55?
            if(std::fabs(owncar.angle) < 30 && std::fabs(owncar.angleSpeed) < 2 && opMaxAngle > 50)
            {
//                 std::cout 
//                 << "Ram time...: "
//                 << "dist: " << dist
//                 << ", bump at angle: " << owncar.angle
//                 << ", crashDist: " << crashDist
//                 << ", ownMaxAngle: " << ownMaxAngle
//                 << std::endl;
                
                if (opSoSlow || m_switchLane != 0) 
                {
                    return false;
                }
                
                //not a slower car, so it is ok.
                m_lockedTarget = opcolor;
                
                return true;
            }
            else
            {
                return false;
            }
            //     std::cout << __LINE__ << std::endl;
            
        }
    }
    
    return false;
}

bool game_logic::carAhead()
{
    std::map<std::string, CarsInfo::Car>::iterator it;
    for (it=m_cInfo.cars.begin(); it!=m_cInfo.cars.end(); it++) {
        if (m_color.compare(it->first) == 0) continue;
        if (it->second.endLane != m_cInfo.cars[m_color].endLane) continue;
        
        
        double dist = m_cInfo.distToCar(m_color, it->first);
        if (dist < 0) continue;
        double speedDiff = m_cInfo.cars[m_color].speedAverage - it->second.speedAverage;
        if (speedDiff < -0.2) continue; // don't overtake faster cars
        if (speedDiff < 1) speedDiff = 1;
        
        if((dist - 40)/speedDiff > 10) continue;
        
        return true;
        // std::cout << it->first << ": " << dist << " lane: " << it->second.endLane << " spDiff: " << speedDiff << " " << speedDist << std::endl;
    }
    return false;
}

bool game_logic::carBehind()
{
    std::map<std::string, CarsInfo::Car>::iterator it;
    for (it=m_cInfo.cars.begin(); it!=m_cInfo.cars.end(); it++) {
        if (m_color.compare(it->first) == 0) continue;
        if (it->second.endLane != m_cInfo.cars[m_color].endLane) continue;
        
        
        double dist = m_cInfo.distToCar(it->first, m_color);
        if (dist < 0) continue;
        double speedDiff = m_cInfo.cars[m_color].speedAverage - it->second.speedAverage;
        if (speedDiff < -0.2) continue; // don't overtake faster cars
        if (speedDiff < 1) speedDiff = 1;
        
        if((dist - 40)/speedDiff > 10) continue;
        
        return true;
        // std::cout << it->first << ": " << dist << " lane: " << it->second.endLane << " spDiff: " << speedDiff << " " << speedDist << std::endl;
    }
    return false;
}

int game_logic::overtake(bool &overtaking)
{
    std::map<std::string, CarsInfo::Car>::iterator it;
    int blockLeft = 0;
    int blockAhead = 0;
    int blockRight = 0;
    
    //find next switch pieces
    unsigned int firstSwitch = m_cInfo.cars[m_color].piece + 1;
    
    if (firstSwitch >= m_tInfo.pieceCount()) firstSwitch = 0;
    
    while (!m_tInfo.hasSwitch(firstSwitch) && firstSwitch != m_cInfo.cars[m_color].piece)
    {
        firstSwitch++;
        if (firstSwitch >= m_tInfo.pieceCount()) firstSwitch = 0;
    }
    
    unsigned int secondSwitch = firstSwitch + 1;
    
    if (secondSwitch >= m_tInfo.pieceCount()) secondSwitch = 0;
    
    while (!m_tInfo.hasSwitch(secondSwitch) && secondSwitch != firstSwitch)
    {
        secondSwitch++;
        if (secondSwitch >= m_tInfo.pieceCount()) secondSwitch = 0;
    }
    
    //     double toFirstSwitchDist = m_tInfo.distToPiece(m_cInfo.cars[m_color].piece, m_cInfo.cars[m_color].inPieceDist, m_cInfo.cars[m_color].startLane, m_cInfo.cars[m_color].endLane, firstSwitch);
    double toSecondSwitchDist = m_tInfo.distToPiece(m_cInfo.cars[m_color].piece, m_cInfo.cars[m_color].inPieceDist, m_cInfo.cars[m_color].startLane, m_cInfo.cars[m_color].endLane, secondSwitch);
    
    // don't try to go off the track
    if (m_cInfo.cars[m_color].endLane == 0) {
        blockLeft = 1000;
    }
    if (m_cInfo.cars[m_color].endLane == m_tInfo.numLanes() -1) {
        blockRight = 1000;
    }
    
    // check the cars
    //TODO: Find the slowest( min(it->second.speed / optimalSpeedIt->second) ), ignore all others
    for (it=m_cInfo.cars.begin(); it!=m_cInfo.cars.end(); it++) {
        if (m_color.compare(it->first) == 0) continue;
        double dist = m_cInfo.distToCar(m_color, it->first);
        
        if(dist < 0 && m_cInfo.distToCar(it->first, m_color) > 40) continue;
            
        if (dist < 0) dist = 40;
        
        
        std::map<double, double>::iterator optimalSpeedIt = m_defaultSpeed.lower_bound(it->second.piece*1000 + it->second.inPieceDist);
        if(optimalSpeedIt == m_defaultSpeed.end())
        {
            optimalSpeedIt--;
        }
               
        //overtake only if the car is 10% slower. 
        //TODO: Maybe this value should be an average of 10 last speeds?
        if ((it->second.speed < optimalSpeedIt->second * 0.85 && 
            dist < toSecondSwitchDist) 
        ) {
            if (it->second.endLane < m_cInfo.cars[m_color].endLane) {
                blockLeft++;
            }
            else if (it->second.endLane > m_cInfo.cars[m_color].endLane) {
                blockRight++;
            }
            else {
                blockAhead++;
            }
        }
        // std::cout << it->first << ": " << dist << " lane: " << it->second.endLane << " spDiff: " << speedDiff << " " << speedDist << std::endl;
    }
    
    //std::cout << " " << blockLeft << " " << blockAhead << " " << blockRight << std::endl;
    
    overtaking = (blockRight > 0 && blockRight < 10) || (blockAhead > 0 && blockAhead < 10) || (blockRight > 0 && blockRight < 10);
    
    if (blockRight < blockLeft && blockRight < blockAhead) {
        return 1;
    }
    if (blockLeft < blockRight && blockLeft < blockAhead) {
        return -1;
    }
    return 0;
}

int game_logic::goSafeLane(bool &safeLaning)
{
    std::map<std::string, CarsInfo::Car>::iterator it;
    int blockLeft = 0;
    int blockAhead = 0;
    int blockRight = 0;
   
    // don't try to go off the track
    if (m_cInfo.cars[m_color].endLane == 0) {
        blockLeft = 1000;
    }
    if (m_cInfo.cars[m_color].endLane == m_tInfo.numLanes() -1) {
        blockRight = 1000;
    }
    
    // check the cars
    for (it=m_cInfo.cars.begin(); it!=m_cInfo.cars.end(); it++) {
        if (m_color.compare(it->first) == 0) continue;
        double dist = m_cInfo.distToCar(it->first, m_color);
        if (dist < 0) continue;
        
        
        std::map<double, double>::iterator optimalSpeedIt = m_defaultSpeed.lower_bound(it->second.piece*1000 + it->second.inPieceDist);
        if(optimalSpeedIt == m_defaultSpeed.end())
        {
            optimalSpeedIt--;
        }
               
//                std::cout 
//                << "Op speed:" << it->second.speed << std::endl
//                << ", Optimal speed * 0.95:" << optimalSpeedIt->second * 0.95<< std::endl
//                << ", dist:" << dist << std::endl
//                << ", color:" << it->first << std::endl
//                << ", piece:" << m_cInfo.cars[m_color].piece << std::endl
//                << std::endl;
               
        if (it->second.speed > optimalSpeedIt->second * 0.95 &&
            dist < 200
        ) {
            if (it->second.endLane < m_cInfo.cars[m_color].endLane) {
                blockLeft++;
            }
            else if (it->second.endLane > m_cInfo.cars[m_color].endLane) {
                blockRight++;
            }
            else {
                blockAhead++;
            }
        }
        // std::cout << it->first << ": " << dist << " lane: " << it->second.endLane << " spDiff: " << speedDiff << " " << speedDist << std::endl;
    }
    
    //std::cout << " " << blockLeft << " " << blockAhead << " " << blockRight << std::endl;
    
    safeLaning = (blockRight > 0 && blockRight < 10) || (blockAhead > 0 && blockAhead < 10) || (blockRight > 0 && blockRight < 10);
    
//     if (blockRight < blockLeft && blockRight < blockAhead) {
//         std::cout << "Safe lane on Right" << std::endl;
//         return 1;
//     }
//     if (blockLeft < blockRight && blockLeft < blockAhead) {
//         std::cout << "Safe lane on Left" << std::endl;
//         return -1;
//     }
    if(safeLaning)
    {
        int laneDir = m_tInfo.reverseSwitchLane(m_cInfo.cars[m_color].piece, m_cInfo.cars[m_color].endLane);
//         std::cout << "Safe lane on: " << m_tInfo.reverseSwitchLane(m_cInfo.cars[m_color].piece, m_cInfo.cars[m_color].endLane) << std::endl;
        return laneDir;
    }
    else
    {
        return 0;
    }
    
}
// kate  space-indent on;
