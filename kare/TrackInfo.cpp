#include "TrackInfo.h"
#include <iostream>
#include <math.h>    // std::sqrt

void TrackInfo::clearTrack()
{
    m_trackPieces.clear();
}

void TrackInfo::addPiece(const jsoncons::json &data)
{
    Piece piece;
    Lane lane;

    //std::cout << data.as<std::string>() << std::endl;
    piece.hasSwitch = data.get("switch", false).as<bool>();
    piece.angle = data.get("angle", 0).as<double>();

    double radius = data.get("radius", 0).as<double>();
    double length = data.get("length", 0).as<double>();
    for (size_t i=0; i<laneCenterOffset.size(); i++) {
        if (radius == 0) {
            // straight
            lane.length = length;
            lane.radius = 1000000000;
        }
        else {
            // FIXME check + or -
            if (piece.angle > 1) {
                lane.radius = radius - laneCenterOffset[i];
            }
            else {
                lane.radius = - radius - laneCenterOffset[i];
            }
            lane.length = (2 * 3.14159265359 * lane.radius * piece.angle) / 360;
        }
        piece.lanes.push_back(lane);
    }
    m_trackPieces.push_back(piece);
}

TrackInfo::Lane TrackInfo::trackLane(size_t pieceIndex, size_t lane) const
{
    if (pieceIndex >= m_trackPieces.size()) {
        return Lane();
    }
    const Piece &piece = m_trackPieces[pieceIndex];
    if (lane >= piece.lanes.size()) {
        return Lane();
    }
    return piece.lanes[lane];
}

double TrackInfo::distToPiece(unsigned int curr_piece, double piece_dist, int start_lane, int end_lane, unsigned int target_piece)
{
    double retVal = 0;
    
    retVal = -piece_dist;
    
    for(unsigned int j = curr_piece; j < (curr_piece < target_piece ? target_piece : target_piece + pieceCount()); ++j)
    {
        int actualIndex = (curr_piece == pieceCount() - 1 ? 0 : curr_piece + 1);
        
        retVal = retVal + length(actualIndex, start_lane, end_lane);
    }
    
    return retVal;
}

double TrackInfo::length(size_t piece, size_t startLane, size_t endLane) const
{
    // FIXME check piece..
    if (startLane != endLane && m_trackPieces[piece].hasSwitch) {
        double lenStart = trackLane(piece, startLane).length;
        double rStart = std::fabs(radius(piece, 0, startLane, startLane));
        double rEnd = std::fabs(radius(piece, 0, endLane, endLane));

        if (radius(piece, 0, startLane, startLane) > 1000) {
            if (std::fabs(lenStart-100) < 0.0001) {
                return 102.060274993;
            }
            else if (std::fabs(lenStart-99) < 0.0001) {
                return 101.08;
            }
            else if (std::fabs(lenStart-90) < 0.0001) {
                return 92.2817;
            }
            else if (std::fabs(lenStart-78) < 0.0001) {
                return 80.619;
            }
        }
        if (rStart == 210 && rEnd == 190  && std::fabs(lenStart-82.4668) < 0.0001) {
            return 81.0539;
        }
        if (rStart == 190 && rEnd == 210  && std::fabs(lenStart-74.6128) < 0.0001) {
            return 81.0547;
        }
        if (rStart == 90 && rEnd == 110  && std::fabs(lenStart-70.6858) < 0.0001) {
            return 81.0295;
        }
        if (rStart == 110 && rEnd == 90  && std::fabs(lenStart-86.3938) < 0.0001) {
            return 81.0281;
        }

        //std::cout << "fallback " << rStart << " " << rEnd << " " << lenStart << std::endl;

        // FIXME: the ones above are known (for now approximate the rest)
        double len1 = trackLane(piece, startLane).length/2;
        double len2 = trackLane(piece, endLane).length/2;
        double lDiff = (laneCenterOffset[startLane] - laneCenterOffset[endLane])/2;
        double diags = std::sqrt(len1*len1 + lDiff*lDiff) + std::sqrt(len2*len2 + lDiff*lDiff);
        //std::cout << startLane << " " << endLane << " " << len << " " << diag <<  std::endl;
        return diags;
    }

    return trackLane(piece, startLane).length;
}

double TrackInfo::radius(size_t piece, double piece_dist, size_t startLane, size_t endLane) const
{
    if(startLane != endLane && m_trackPieces[piece].hasSwitch)
    {
        double piece_length = length(piece, startLane, endLane);
        if(piece_dist < piece_length / 2)
        {
            return trackLane(piece, startLane).radius;
        }
        else
        {
            return trackLane(piece, endLane).radius;
        }
    }
    return trackLane(piece, startLane).radius; // FIXME check if we need to add a switch radius
}

double TrackInfo::angle(size_t piece) const
{
    if (piece >= m_trackPieces.size()) {
        return false;
    }
    return m_trackPieces[piece].angle;
}

bool TrackInfo::hasSwitch(size_t piece) const
{
    if (piece >= m_trackPieces.size()) {
        return false;
    }
    return m_trackPieces[piece].hasSwitch;
}


void TrackInfo::calculateTurbo()
{
    // go to the first curve
    int piece = 0;
    while (piece<(int)m_trackPieces.size()) {
        if (radius(piece,0, 0, 0)) {
            break;
        }
        piece++;
    }

    // longest straight
    int turboPiece;
    double turboLen = 0;
    double len = 0;
    for (int i=0; i<(int)m_trackPieces.size()*2; i++) {
        if (radius(piece,0,0,0) > 1000) {
            len += length(piece, 0,0);
            if (len > turboLen) {
                turboLen = len;
                turboPiece = piece;
            }
        }
        else {
            len = 0;
        }
        piece--; if (piece < 0) piece = m_trackPieces.size()-1;
    }

    //std::cout << "Turbo piece" << turboPiece << std::endl;
    m_trackPieces[turboPiece].turboHere = true;
}


bool TrackInfo::useTurbo(size_t piece) const
{
    if (piece >= m_trackPieces.size()) {
        return false;
    }

    return m_trackPieces[piece].turboHere;;
}

int TrackInfo::switchLane(size_t piece, int lane) const
{
    // calculate next switch piece
    size_t sPiece = piece;
    // look forward to the first switch
    while (1) {
        sPiece++; if (sPiece >= m_trackPieces.size()) sPiece=0;
        if (m_trackPieces[sPiece].hasSwitch) {
            break;
        }
    }
    sPiece++; if (sPiece >= m_trackPieces.size()) sPiece=0;

    int currLane = lane;
    // calculate current lane
    size_t p = sPiece;
    double lengthThis = 0;
    while (!m_trackPieces[p].hasSwitch) {
        lengthThis += length(p, currLane, currLane);
        p++; if (p >= m_trackPieces.size()) p=0;
    }
    lengthThis += length(p, lane, lane);

    // left
    double lengthLeft = 0;
    if (lane == 0) {
        lengthLeft = lengthThis;
    }
    else {
        p = sPiece;
        currLane = lane-1;
        while (!m_trackPieces[p].hasSwitch) {
            lengthLeft += length(p, currLane, currLane);
            p++; if (p >= m_trackPieces.size()) p=0;
        }
        lengthLeft += length(p, currLane, currLane);
    }
    // right
    double lengthRight = 0;
    if (lane == (int)laneCenterOffset.size()-1) {
        lengthRight = lengthThis;
    }
    else {
        p = sPiece;
        currLane = lane+1;
        while (!m_trackPieces[p].hasSwitch) {
            lengthRight += length(p, currLane, currLane);
            p++; if (p >= m_trackPieces.size()) p=0;
        }
        lengthRight += length(p, currLane, currLane);
    }

    if (lengthLeft < lengthThis) return -1;
    if (lengthRight < lengthThis) return 1;
    
    if(lengthLeft == lengthThis && lengthRight == lengthThis)
    {
        return switchLane(sPiece, lane);
    }
    
    return 0;
}

int TrackInfo::reverseSwitchLane(size_t piece, int lane) const
{
    // calculate next switch piece
    size_t sPiece = piece;
    // look forward to the first switch
    while (1) {
        sPiece++; if (sPiece >= m_trackPieces.size()) sPiece=0;
        if (m_trackPieces[sPiece].hasSwitch) {
            break;
        }
    }
    sPiece++; if (sPiece >= m_trackPieces.size()) sPiece=0;

    int currLane = lane;
    // calculate current lane
    size_t p = sPiece;
    double lengthThis = 0;
    while (!m_trackPieces[p].hasSwitch) {
        lengthThis += length(p, currLane, currLane);
        p++; if (p >= m_trackPieces.size()) p=0;
    }
    lengthThis += length(p, lane, lane);

    // left
    double lengthLeft = 0;
    if (lane == 0) {
        lengthLeft = lengthThis;
    }
    else {
        p = sPiece;
        currLane = lane-1;
        while (!m_trackPieces[p].hasSwitch) {
            lengthLeft += length(p, currLane, currLane);
            p++; if (p >= m_trackPieces.size()) p=0;
        }
        lengthLeft += length(p, currLane, currLane);
    }
    // right
    double lengthRight = 0;
    if (lane == (int)laneCenterOffset.size()-1) {
        lengthRight = lengthThis;
    }
    else {
        p = sPiece;
        currLane = lane+1;
        while (!m_trackPieces[p].hasSwitch) {
            lengthRight += length(p, currLane, currLane);
            p++; if (p >= m_trackPieces.size()) p=0;
        }
        lengthRight += length(p, currLane, currLane);
    }

    if (lengthLeft > lengthThis) return -1;
    if (lengthRight > lengthThis) return 1;
    
    if(lengthLeft == lengthThis && lengthRight == lengthThis)
    {
        return reverseSwitchLane(sPiece, lane);
    }
    
    return 0;
}