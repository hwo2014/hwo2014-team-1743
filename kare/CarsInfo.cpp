#include "CarsInfo.h"
#include <math.h>
#include <iostream>


void CarsInfo::updateCars(const jsoncons::json &data)
{
    Car car;

    for (size_t i=0; i<data.size(); i++) {
        const auto &carMap = data[i];
        car.angle = carMap["angle"].as<double>();

        const auto &id = carMap["id"];
        car.name = id["name"].as<std::string>();
        car.color = id["color"].as<std::string>();

        const auto &piecePos = carMap["piecePosition"];
        car.piece = piecePos["pieceIndex"].as<int>();
        car.inPieceDist = piecePos["inPieceDistance"].as<double>();
        car.lap = piecePos["lap"].as<int>();

        const auto &lane = piecePos["lane"];
        car.startLane = lane["startLaneIndex"].as<int>();
        car.endLane = lane["endLaneIndex"].as<int>();

        // calculate the speed
        Car oldCar = cars[car.color];
        if (car.piece + (car.lap*1000) > oldCar.piece + (oldCar.lap*1000)) {
            double prevPieceLen = m_track.length(oldCar.piece, oldCar.startLane, oldCar.endLane);
            //double prevPieceLenStart = m_track.length(oldCar.piece, oldCar.startLane, oldCar.startLane);
            //double prevPieceLenEnd = m_track.length(oldCar.piece, oldCar.endLane, oldCar.endLane);
            double prevLength = prevPieceLen - oldCar.inPieceDist;
            car.speed = car.inPieceDist + prevLength;
            //std::cout << "inPiece old " << oldCar.inPieceDist << " new " << car.inPieceDist << std::endl;
            //std::cout << "speed " << oldCar.speed << " " << car.speed << std::endl;
            //std::cout << "lens " << prevPieceLenStart << " " << prevPieceLen << " " << prevPieceLenEnd << std::endl;
            if (abs(car.speed - oldCar.speed) > oldCar.speed/10) {
                std::cout << "---" << m_track.length(oldCar.piece, oldCar.startLane, oldCar.startLane)
                << " " << m_track.length(oldCar.piece, oldCar.startLane, oldCar.endLane)
                << " --- " << car.inPieceDist << " " << oldCar.inPieceDist << " " << prevLength << std::endl;
            }
        }
        else {
            car.speed = car.inPieceDist - oldCar.inPieceDist;
        }

        //if (qAbs(car.speed - oldCar.speed) > oldCar.speed/1.5) {
        //    qDebug() << "---" << m_track.length(oldCar.piece, oldCar.endLane) << car.inPieceDist << oldCar.inPieceDist;
        //    qDebug() << "---" << oldCar.piece << oldCar.lap << car.piece << car.lap;
        //}

        // calculate the acc;
        car.acc = car.speed - oldCar.speed;

        // angle velocity and acceleration
        car.angleSpeed = car.angle - oldCar.angle;
        car.angleAcc = car.angleSpeed - oldCar.angleSpeed*0.9;

        // average speed
        if (car.speed > 0 && oldCar.speedAverage <0.001) {
            car.speedAverage = car.speed;
        }
        else if (car.speed < 0.001) {
            car.speedAverage = 0;
        }
        else {
            car.speedAverage = oldCar.speedAverage + (car.speed - oldCar.speedAverage) / 70;
        }

        // replace the old car
        cars[car.color] = car;
    }
}


double CarsInfo::distToCar(const std::string &fromColor, const std::string &toColor)
{
    if (cars.count(fromColor)==0 || cars.count(toColor)==0) {
        return 0;
    }
    const Car &from = cars[fromColor];
    const Car &to = cars[toColor];
    
    return distToCar(from, to);
    
}

double CarsInfo::distToCar(const CarsInfo::Car &from, const CarsInfo::Car &to)
{
    double dist = 0;
    size_t calcPiece = from.piece;

    size_t pieceDiff = to.piece;
    if (pieceDiff < from.piece) {
        pieceDiff += m_track.pieceCount();
    }
    pieceDiff -= from.piece;
    if (pieceDiff > 20) {
        //the car is behind us
        return -1;
    }

    if (pieceDiff == 0) {
        return to.inPieceDist - from.inPieceDist;
    }

    dist += m_track.length(from.piece, from.startLane, from.endLane) - from.inPieceDist;
    calcPiece++;
    if (calcPiece >= m_track.pieceCount()) calcPiece = 0;

    while (calcPiece != to.piece) {
        dist += m_track.length(calcPiece, from.startLane, from.startLane);
        calcPiece++;
        if (calcPiece >= m_track.pieceCount()) calcPiece = 0;
        if (calcPiece == from.piece) {
            std::cout << "error in calculations" << calcPiece << " " << from.piece << " " << to.piece << std::endl;
            return -1;
        }
    }

    dist += to.inPieceDist;
    return dist;
}

double CarsInfo::averageSpeedDiff(const std::string &fromColor, const std::string &toColor)
{
    if (cars.count(fromColor)==0 || cars.count(toColor)==0) {
        return 0;
    }
    return cars[toColor].speedAverage -cars[fromColor].speedAverage;
}



