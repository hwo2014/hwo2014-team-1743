#ifndef RobotLogic_h
#define RobotLogic_h

#include <QObject>
#include <QByteArray>
#include <QString>
#include <QVariantList>
#include <QVariantMap>

#include "TrackInfo.h"
#include "CarsInfo.h"

class RobotLogic : public QObject
{
    Q_OBJECT
public:
    RobotLogic(QObject *parent = 0);

public Q_SLOTS:
    void recievedMsg(const QByteArray &data);
    void sendJoin(const QString &name, const QString &key);
    void sendJoinRace(const QString &name, const QString &key, const QString &track, const QString &passwd, int count);
    void sendCreateRace(const QString &name, const QString &key, const QString &track, const QString &passwd, int count);
    void sendThrottle(double throttle, int tick);
    void sendSwitchLane(bool left);
    void sendPing();

Q_SIGNALS:
    void sendMsg(const QByteArray &data);
    void quit();

private:
    void handleJoin(const QVariantMap &data);
    void handleYourCar(const QVariantMap &data);
    void handleGameInit(const QVariantMap &data);
    void handleGameStart(const QVariantMap &data);
    void handleCarsPos(const QVariantList &data, int tick);

    double newMaximalThrottle(const CarsInfo::Car &car);

    TrackInfo m_tInfo;
    CarsInfo m_cInfo;

    QString m_color;

    int m_laps;
    int m_maxLapTimeMs;
    bool m_quickRace;

    double m_torque; // The normal acceleration to exceed to change the angle.
    double m_damping; // The damping multiplier that stops the oscillations.
    double m_springConst; // The spring constant that forces the angle back.
    double m_multiplier; // A magic multiplier that converts the forces to angles.
    double m_maxSpeed; // The maximum speed.
    int    m_turboCounter;  // The ticks remaining with turbo.
    double m_turboMultip; // The maxSpeed turbo multiplier.
    int    m_turboAvailable; // shows how many ticks turbo is available.
};

#endif
