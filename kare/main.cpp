#include <iostream>
#include <string>
#include <jsoncons/json.hpp>
#include "protocol.h"
#include "connection.h"
#include "game_logic.h"
#include <boost/timer.hpp>

using namespace hwo_protocol;

void run(hwo_connection& connection, const std::string& name, const std::string& key, const std::string& track , const std::string& passwd, int count, bool create)
{
  game_logic game;

  if (track.size() == 0) {
      connection.send_requests({ make_join(name, key) });
  }
  else if (!create) {
      connection.send_requests({ makeJoinRace(name, key, track, passwd, count) });
  }
  else {
      connection.send_requests({ makeCreateRace(name, key, track, passwd, count) });
  }

  for (;;)
  {
    boost::system::error_code error;
    auto response = connection.receive_response(error);

    if (error == boost::asio::error::eof)
    {
      std::cout << "Connection closed" << std::endl;
      break;
    }
    else if (error)
    {
      throw boost::system::system_error(error);
    }

//     boost::timer t;
    const std::vector<jsoncons::json>& reaction = game.react(response);
//     std::cout << "Processing data took: " << t.elapsed() << "s" << std::endl;
    std::string msgType = reaction[0]["msgType"].as<std::string>();
    //std::cout << "msgs:: "<< response["msgType"].as<std::string>() << ":" << reaction[0].as<std::string>() << std::endl;
    if(msgType.compare("skip") != 0)
    {
        //         std::cout << "Sending: " << msgType << std::endl;
        connection.send_requests(reaction);
    }
  }
}

int main(int argc, const char* argv[])
{
  try
  {
    if (argc < 5)
    {
      std::cerr << "Usage: ./run host port botname botkey" << std::endl;
      return 1;
    }

    const std::string host(argv[1]);
    const std::string port(argv[2]);
    const std::string name(argv[3]);
    const std::string key(argv[4]);
    std::string track;
    std::string passwd = "";
    int count = 1;
    bool create = false;

    if (argc > 5) {
        track = std::string(argv[5]);
    }
    if (argc > 6) {
        passwd = std::string(argv[6]);
    }
    if (argc > 7) {
        count = atoi(argv[7]);
    }
    if (argc == 9) {
        create = true;
    }
    std::cout << "Host: " << host << ", port: " << port << ", name: " << name << ", key:" << key << ", track: " << track << ", passwd: " << passwd <<  std::endl;

//     for(;;)
//     {
        hwo_connection connection(host, port);
        run(connection, name, key, track, passwd, count, create);
//     }
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
    return 2;
  }

  return 0;
}
