#include "RobotLogic.h"
#include <QDebug>
#include <qjson/parser.h>
#include <qjson/serializer.h>

static const double speedDivisor = 50;

RobotLogic::RobotLogic(QObject *parent)
: QObject(parent)
,m_cInfo(m_tInfo)
,m_torque(0.32) // FIXME these are tested out constants that can vary
,m_damping(0.9)
,m_springConst(0.0135)
,m_multiplier(0.8)
,m_maxSpeed(10) // check the maxSpeed at the beginning
,m_turboCounter(0)
,m_turboMultip(1)
,m_turboAvailable(0)
{
}

void RobotLogic::recievedMsg(const QByteArray &data)
{
    QJson::Parser parser;
    QVariantMap msg = parser.parse (data).toMap();

    QString type = msg["msgType"].as<std::string>();

    if (type == "carPositions")    handleCarsPos(msg["data"].toList(), msg["gameTick"].as<int>());
    else if (type == "join")       handleJoin(msg["data"].toMap());
    else if (type == "yourCar")    handleYourCar(msg["data"].toMap());
    else if (type == "gameInit")   handleGameInit(msg["data"].toMap());
    else if (type == "gameStart")  handleGameStart(msg["data"].toMap());
    else {
        qDebug() << type;
        sendPing();
    }
}

void RobotLogic::sendJoin(const QString &name, const QString &key)
{
    QJson::Serializer serializer;
    QVariantMap data;
    data.insert("name", name);
    data.insert("key", key);

    QVariantMap msg;
    msg.insert("msgType", "join");
    msg.insert("data", data);

    QByteArray json = serializer.serialize(msg);
    emit sendMsg(json);
}

void RobotLogic::sendJoinRace(const QString &name, const QString &key, const QString &track, const QString &passwd, int count)
{
    QJson::Serializer serializer;
    QVariantMap botId;
    botId.insert("name", name);
    botId.insert("key", key);

    QVariantMap data;
    data.insert("trackName", track);
    data.insert("password", passwd);
    data.insert("carCount", count);
    data.insert("botId", botId);


    QVariantMap msg;
    msg.insert("msgType", "joinRace");
    msg.insert("data", data);

    QByteArray json = serializer.serialize(msg);
    emit sendMsg(json);
}

void RobotLogic::sendCreateRace(const QString &name, const QString &key, const QString &track, const QString &passwd, int count)
{
    QJson::Serializer serializer;
    QVariantMap botId;
    botId.insert("name", name);
    botId.insert("key", key);

    QVariantMap data;
    data.insert("trackName", track);
    data.insert("password", passwd);
    data.insert("carCount", count);
    data.insert("botId", botId);


    QVariantMap msg;
    msg.insert("msgType", "joinRace");
    msg.insert("data", data);

    QByteArray json = serializer.serialize(msg);
    emit sendMsg(json);
}

void RobotLogic::sendThrottle(double throttle, int tick)
{
    QJson::Serializer serializer;
    QVariantMap msg;
    msg.insert("msgType", "throttle");
    msg.insert("data", throttle);
    //msg.insert("gameTick", tick);

    QByteArray json = serializer.serialize(msg);
    emit sendMsg(json);
}

void RobotLogic::sendSwitchLane(bool left)
{
    QJson::Serializer serializer;
    QVariantMap msg;
    msg.insert("msgType", "switchLane");
    msg.insert("data", left ? "Left" : "Right");

    QByteArray json = serializer.serialize(msg);
    emit sendMsg(json);
}

void RobotLogic::sendPing()
{
    emit sendMsg("{\"msgType\": \"ping\"}\n");
}


void RobotLogic::handleJoin(const QVariantMap &/*data*/)
{
}

void RobotLogic::handleYourCar(const QVariantMap &data)
{
    m_color = data["color"].as<std::string>();
}

void RobotLogic::handleGameInit(const QVariantMap &data)
{
    //qDebug() << "gameInit"; // data;
    QVariantMap race = data["race"].toMap();

    // Race Session
    QVariantMap raceSession = race["raceSession"].toMap();
    m_laps = raceSession["laps"].as<int>();
    m_maxLapTimeMs = raceSession["maxLapTimeMs"].as<int>();
    m_quickRace = raceSession["quickRace"].toBool();

    // FIXME cars

    // Track
    QVariantMap track = race["track"].toMap();

    m_tInfo.id = track["id"].as<std::string>();
    m_tInfo.name = track["name"].as<std::string>();

    // Lanes
    QVariantList lanes = track["lanes"].toList();
    m_tInfo.laneCenterOffset.resize(lanes.size());
    for (int i=0; i<lanes.size(); i++) {
        int index = lanes[i].toMap()["index"].as<int>();
        if (index < 0 || index > lanes.size()) {
            qDebug() << "list index out of range";
            continue;
        }
        double dist = lanes[i].toMap()["distanceFromCenter"].as<double>();
        m_tInfo.laneCenterOffset[index] = dist;
    }

    // pieces
    QVariantList pieces = track["pieces"].toList();
    for (int i=0; i<pieces.size(); i++) {
        m_tInfo.addPiece(pieces[i].toMap());
    }

}

void RobotLogic::handleGameStart(const QVariantMap &data)
{
    qDebug() << "gameStart" << data;
    sendPing();
}

void RobotLogic::handleCarsPos(const QVariantList &data, int tick)
{
    m_cInfo.updateCars(data);

    CarsInfo::Car car = m_cInfo.cars[m_color];
    double throttle = newMaximalThrottle(car);
    qDebug() << tick << car.speed << car.acc << car.slideWidth << car.slideSpeed
    << m_tInfo.radius(car.piece, car.startLane, car.endLane) << throttle;

    // FIXME lane switching
    // FIXME overtaking
    // FIXME turbo

    //if (m_tInfo.radius(car.piece, car.startLane, car.endLane) < 1000 && car.angle > 0) {
        sendThrottle(throttle, tick);
    //}

    //else {
    //    sendThrottle(1);
    //}
}

double RobotLogic::newMaximalThrottle(const CarsInfo::Car &ccar)
{
    CarsInfo::Car car = ccar;
    double maxSlide = 0;

    if (car.speed == 0 && m_maxSpeed == 0) {
        return 1.0;
    }

    if (car.speed > 0 && m_maxSpeed == 0) {
        m_maxSpeed = car.speed * speedDivisor; // 50 is a magic constant ;)
        qDebug() << m_maxSpeed;
    }

    // Simulate what happens if we now set throttle 1.0 and then 0.0 for x ticks
    // if the angle goes over 60 then return 0 otherwise return 1.0

    // first add speed for maxxpeed
    car.acc = (m_maxSpeed*m_turboMultip - car.speed)/speedDivisor;
    car.speed += car.acc;

    while (car.speed > 4) { // FIXME find the tightest curve and calculate the slip speed -> limit

        car.inPieceDist += car.speed;

        if (car.inPieceDist > m_tInfo.length(car.piece, car.startLane, car.endLane)) {
            car.inPieceDist -= m_tInfo.length(car.piece, car.startLane, car.endLane);
            car.piece++;
            if (car.piece >= m_tInfo.pieceCount()) {
                car.lap++;
                car.piece = 0;
            }
        }

        // normal acceleration mass == 1
        double normalAcc = car.speed * car.speed / m_tInfo.radius(car.piece, car.startLane, car.endLane);
        if (normalAcc > m_torque) {
            normalAcc -= m_torque;
        }
        else if (normalAcc < - m_torque) {
            normalAcc += m_torque;
        }
        else {
            normalAcc = 0;
        }

        normalAcc *= m_multiplier;

        double springAcc = car.slideWidth * m_springConst;

        car.slideAcc = normalAcc - springAcc;

        car.slideSpeed += car.slideAcc;
        car.slideSpeed *= m_damping;

        car.slideWidth += car.slideSpeed;

        maxSlide = qMax(qAbs(car.slideWidth), maxSlide);

        //qDebug() << "---" << car.speed << car.acc << car.slideWidth << car.slideSpeed
        //<< m_tInfo.radius(car.piece, car.startLane, car.endLane);

        if (maxSlide > 7.5) {
            return 0;
        }
        // now decrease the speed until we are done
        car.acc = car.speed/speedDivisor;
        car.speed -= car.acc;
    }

    qDebug() << maxSlide;

    // no crash -> full speed ahead :)
    return 1;
}


