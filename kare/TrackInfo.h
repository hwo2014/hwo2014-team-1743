#ifndef TrackInfo_h
#define TrackInfo_h

#include <vector>
#include <string>
#include <map>
#include <iostream>
#include <jsoncons/json.hpp>


class TrackInfo {
public:
    class Lane {
    public:
        Lane(): length(0), radius(0) {}
        Lane(const Lane &o): length(o.length), radius(o.radius) {}
        const Lane &operator=(const Lane &other) {
            if (&other == this) return *this;
            length = other.length;
            radius = other.radius;
        }
        double length;
        double radius;
    };

    class Piece {
    public:
        Piece() : turboHere(false), hasSwitch(false), angle(0) {}
        Piece(const Piece &other) : turboHere(other.turboHere), hasSwitch(other.hasSwitch), angle(other.angle), lanes(other.lanes) {}
        const Piece &operator=(const Piece &other) {
            if (&other == this) return *this;
            turboHere = other.turboHere;
            hasSwitch = other.hasSwitch;
            angle = other.angle;
            lanes = other.lanes;
            return *this;
        }
        bool turboHere;
        bool hasSwitch;
        float angle;
        std::vector<Lane> lanes;
    };

    void addPiece(const jsoncons::json &data);

    double distToPiece(unsigned int curr_piece, double piece_dist, int start_lane, int end_lane, unsigned int target_piece);
    double length(size_t piece, size_t startLane, size_t endLane) const;
    double radius(size_t piece, double piece_dist, size_t startLane, size_t endLane) const;
    double angle(size_t piece) const;
    bool hasSwitch(size_t piece) const;
    bool useTurbo(size_t piece) const;
    int switchLane(size_t piece, int lane) const;
    int reverseSwitchLane(size_t piece, int lane) const;

    void calculateTurbo();
    void clearTrack();

    size_t numLanes() const { return (int)laneCenterOffset.size(); }

    std::string id;
    std::string name;
    std::vector<double> laneCenterOffset;

    size_t pieceCount() const {return m_trackPieces.size(); }

private:
    Lane trackLane(size_t piece, size_t lane) const;
    std::vector<Piece> m_trackPieces;
};

#endif

